import {Injectable} from '@angular/core';
import {SignInRequest} from '@core/authentication/dto/SignInRequest';
import {User} from '@core/models/User';
import {HttpInvokeService} from '@core/services/http-invoke.service';
import {SignInResponse} from '@core/authentication/dto/SignInResponse';
import {ServicesSignatures} from '@shared/enums/services-signatures.enum';
import {map} from 'rxjs/operators';
import {Paths} from '@shared/enums/paths.enum';
import {Router} from '@angular/router';
import * as fromApp from '@state/auth/index';
import {Store} from '@ngrx/store';
import * as AuthActions from '@state/auth/auth.actions';
import {ChangePasswordInterface} from '@app/features/password/interfaces/change-password.interface';
import {Observable} from 'rxjs';
import {UserForgotPsw} from '@core/authentication/interfaces/user-forgot-psw.interface';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public static currentUser: User;

  constructor(
    private router: Router,
    private httpInvokeService: HttpInvokeService,
    private store: Store<fromApp.AuthState>

  ) { }

  public static setToken(data: string) {
    localStorage.setItem('token', data);
  }

  public static setPswNew(data: boolean) {
    localStorage.setItem('vp', btoa(data.toString()));
  }

  public static getToken(): string {
    return localStorage.getItem('token');
  }

  public static getPswNew(): string {
    return atob(localStorage.getItem('vp'));
  }

  public static isLogin(): boolean {
    const userPayload = AuthService.getUserTimeToken();
    if (userPayload) {
      return userPayload.exp > Date.now() / 1000;
    } else {
      return false;
    }
  }

  public static isNewPsw(): boolean {
    const newPsw = AuthService.getPswNew();
    return newPsw === 'true';
  }

  static getUserTimeToken() {
    const token = localStorage.getItem('token');
    if (token) {
      const userPayload = atob(token.split('.')[1]);
      return JSON.parse(userPayload);
    } else {
      return null;
    }
  }

  public signIn(userData: SignInRequest) {
    return this.httpInvokeService
      .post< SignInRequest, SignInResponse >('/authenticate', userData, ServicesSignatures.SIGN_IN, true)
      .pipe(map( data => {
        AuthService.setToken(data.token);
        AuthService.setPswNew(data.change);
        const user = new User( data.name, data.rol);
        user.change = data.change;
        AuthService.currentUser = user;
        return user;
      }));
  }

  public logout() {
    localStorage.clear();
    AuthService.currentUser = null;
    this.store.dispatch(AuthActions.ClearStoreAction());
    this.router.navigate([Paths.AUTH], {replaceUrl: true});
  }

  public changePassword(userData: ChangePasswordInterface): Observable<any> {
    return this.httpInvokeService
      .post<ChangePasswordInterface, any>('/changepsw', userData, ServicesSignatures.CHANGE_PSW, true)
      .pipe(map( () => {
        AuthService.setPswNew(false);
      }));
  }

  public forgotPassword(data: UserForgotPsw): Observable<any> {
    return this.httpInvokeService
      .post<UserForgotPsw, any>( '/forgotpsw', data, ServicesSignatures.FORGOT_PSW, true);
  }

}
