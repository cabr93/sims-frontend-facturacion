import { Injectable } from '@angular/core';
import {NzModalRef, NzModalService} from 'ng-zorro-antd';
import {TranslateService} from '@ngx-translate/core';
import idleService, { IdleEvents } from '@kurtz1993/idle-service';
import {AuthService} from '@core/services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class IdleService {

  private modalTimeout: NzModalRef;

  private counter: number;

  constructor(
    private modalService: NzModalService,
    private translateService: TranslateService,
    private authService: AuthService

) { }

  public start(sessionTime: number) {
    this.counter = 10;
    const { title, label, seconds } = this.translateService.instant('timeoutLabels');

    idleService.configure({
      timeToIdle: 60 * sessionTime,
      timeToTimeout: 10,
      autoResume: true,
      listenFor: 'click mousemove',
    });

    idleService.on(IdleEvents.UserIsBack, () => {
      this.counter = 10;
      this.modalTimeout.destroy();
      this.modalTimeout = null;

    });

    idleService.on(IdleEvents.TimeoutWarning, countdown => {
      this.counter -= 1;
      const myThis = this;
      if (!this.modalTimeout) {
        this.modalTimeout = this.modalService.create({
          nzTitle: title,
          nzContent: `<div>${label}<strong>${this.counter}</strong>${seconds}</p>`,
          nzFooter: [
            {
              label: 'Cancelar',
              type: 'default',
              onClick(): void {
                myThis.counter = 10;
                myThis.modalTimeout.destroy();
                myThis.modalTimeout = null;
              }
            }
          ]
        });
      } else {
        this.modalTimeout.getElement().querySelector('strong').innerText = String(this.counter);
      }
    });

    idleService.on(IdleEvents.UserHasTimedOut, () => {
      this.modalTimeout.destroy();
      this.modalTimeout = null;
      this.authService.logout();
      this.counter = 10;
    });

    idleService.start();
  }

  public stop() {
    idleService.stop();
  }
}
