import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {ToastService} from '@core/services/toast.service';
import {MessageType} from '@core/enums/message-type';
import {environment} from '@env/environment';

@Injectable({
  providedIn: 'root'
})
export class HttpInvokeService {

  private backendUrl: string = environment.backendUrl;

  constructor(
    private httpClient: HttpClient,
    private toastService: ToastService
  ) { }

  public get<T>(request: string, signature: string= '', handleError: boolean = true): Observable<T> {
    return this.methodDispatcher(this.httpClient.get<T>(`${this.backendUrl}${request}`), signature, handleError);
  }

  public post<X, T>(request: string, body: X, signature: string= '', handleError: boolean = true): Observable<T> {
    const headers = new HttpHeaders({'Content-Type': 'application/json'});
    return this.methodDispatcher(this.httpClient.post<T>(`${this.backendUrl}${request}`, body, {headers}), signature, handleError);
  }

  public put<X, T>(request: string, body: X, signature: string= '', handleError: boolean = true): Observable<T> {
    const headers = new HttpHeaders({'Content-Type': 'application/json'});
    return this.methodDispatcher(this.httpClient.put<T>(`${this.backendUrl}${request}`, body, {headers}), signature, handleError);
  }

  public delete<X, T>(request: string, signature: string = '', handleError: boolean = true): Observable<T> {
    return this.methodDispatcher(this.httpClient.delete<T>(`${this.backendUrl}${request}`), signature, handleError);
  }

  public methodDispatcher<T>(obs: Observable<any>, signature: string, handleError: boolean = true): Observable<T> {
    return obs.pipe(catchError(this.handleError.bind(this, signature, handleError)));
  }
  private handleError(signature: string, handleError: boolean, error: HttpErrorResponse): Observable<any> {
    let key: string;
    if (!handleError) {
      return throwError(error.status);
    }
    switch (error.status) {
      case 0:
        key = 'NO_INTERNET';
        error = { ...error, status: null };
        break;
      case 500:
        key = 'defaultError';
        error = { ...error, status: null};
        break;
    }
    if (error.status) {
      this.toastService.showMessage(MessageType.ERROR, `${signature}_${error.status}`);
    } else {
      this.toastService.showMessage(MessageType.ERROR, key);
    }
    return throwError({});
  }
}
