import { Injectable } from '@angular/core';
import {MessageType} from '@core/enums/message-type';
import {TranslateService} from '@ngx-translate/core';
import {NzMessageService} from 'ng-zorro-antd';

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  constructor(
    private translateService: TranslateService,
    private message: NzMessageService
  ) { }

  public showMessage(type: MessageType, key: string = 'defaultError') {
    const txt = this.translateService.instant(key);
    const options = {
      nzDuration: 0
    };

    switch (type) {
      case MessageType.SUCCESS:
        this.message.success(txt);
        break;
      case MessageType.ERROR:
        this.message.error(txt);
        break;
      case MessageType.WARNING:
        this.message.warning(txt);
        break;
      case MessageType.LOADING:
        this.message.loading(txt, options);
        break;
      default:
        this.message.info(txt);
        break;
    }
  }
  public hideMessage() {
    this.message.remove();
  }
}
