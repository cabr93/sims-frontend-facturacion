import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthenticationComponent } from './authentication.component';
import { SignInComponent } from './containers/sign-in/sign-in.component';
import {AuthenticationRoutingModule} from '@core/authentication/authentication-routing.module';
import {SharedModule} from '@shared/shared.module';
import { SignInFormComponent } from './components/sign-in-form/sign-in-form.component';
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';


const entryComponents = [
  SignInFormComponent,
  ForgotPasswordComponent
];

@NgModule({
  declarations: [
    AuthenticationComponent,
    ...entryComponents,
    SignInComponent,
    ForgotPasswordComponent
  ],
  entryComponents: [
    ...entryComponents
  ],
  imports: [
    CommonModule,
    AuthenticationRoutingModule,
    SharedModule
  ]
})
export class AuthenticationModule { }
