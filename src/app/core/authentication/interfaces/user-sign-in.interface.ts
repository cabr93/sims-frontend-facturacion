/**
 * Estructura del formulario de inicio de sesión.
 */
export interface UserSignIn {
  email: string;
  password: string;
}
