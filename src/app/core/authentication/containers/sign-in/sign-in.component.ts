import { Component, OnInit } from '@angular/core';
import {UserSignIn} from '@core/authentication/interfaces/user-sign-in.interface';
import {ServicesStatus} from '@state/services-status/services-status.interface';
import {select, Store} from '@ngrx/store';
import * as fromAuth from '@state/auth';
import {ServicesSignatures} from '@shared/enums/services-signatures.enum';
import * as ServicesStatusSelectors from '@state/services-status/services-status.selector';
import {Subscription} from 'rxjs';
import * as ServicesStatusActions from '@state/services-status/services-status.actions';
import * as AuthActions from '@state/auth/auth.actions';
import {NzModalService} from 'ng-zorro-antd';
import {ForgotPasswordComponent} from '@core/authentication/components/forgot-password/forgot-password.component';


@Component({
  selector: 'fact-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {


  private subscription: Subscription;

  public signInStatus: ServicesStatus;

  constructor(
    private store: Store<fromAuth.State>,
    private modalService: NzModalService
  ) { }

  ngOnInit() {
    this.subscription = this.store.pipe(select(ServicesStatusSelectors.getServiceStateById, { signature: ServicesSignatures.SIGN_IN }))
      .subscribe(status => {
        this.signInStatus = status;
      });
  }

  /**
   * Método encargado de disparar las Actions de:
   * - Ha iniciado el consumo del servicio de iniciar sesión.
   * - Consumir el servicio de olvidar contraseña.
   *
   * @param {UserSignIn} userData El formulario que es capturado en el componente de inicio de sesión hijo.
   */
  public signIn(userData: UserSignIn): void {
    this.store.dispatch(ServicesStatusActions.ShowLoadingAction({ signature: ServicesSignatures.SIGN_IN }));
    const action = AuthActions.SignInAction({ userReq: userData, signature: ServicesSignatures.SIGN_IN });
    this.store.dispatch(action);
  }

  forgotPsw() {
    this.modalService.create({
      nzContent: ForgotPasswordComponent,
      nzClassName: 'app-add-edit-form'
    });
  }
}
