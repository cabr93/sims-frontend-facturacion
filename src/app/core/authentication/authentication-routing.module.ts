import { NgModule } from '@angular/core';
import { RouterModule, Routes} from '@angular/router';
import {AuthenticationComponent} from '@core/authentication/authentication.component';
import {SignInComponent} from '@core/authentication/containers/sign-in/sign-in.component';



const routes: Routes = [
  {
    path: '',
    component: AuthenticationComponent,
    children: [
      { path: '', redirectTo: 'sign-in', pathMatch: 'full'},
      { path: 'sign-in', component: SignInComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class AuthenticationRoutingModule { }
