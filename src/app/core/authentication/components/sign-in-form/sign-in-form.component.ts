import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {UserSignIn} from '@core/authentication/interfaces/user-sign-in.interface';

@Component({
  selector: 'fact-sign-in-form',
  templateUrl: './sign-in-form.component.html',
  styleUrls: ['./sign-in-form.component.scss']
})
export class SignInFormComponent implements OnInit {

  /**
   * Propiedad que indica si se está consumiendo el servicio de inicio de sesión.
   */
  @Input()
  isLoading: boolean;

  /**
   * Evento que se emite el formulario de inicio de sesión.
   */
  @Output()
  emitterSignIn: EventEmitter<UserSignIn> = new EventEmitter<UserSignIn>();

  @Output()
  emitterForgotPsw: EventEmitter<string> = new EventEmitter<string>();

  public signInForm: FormGroup;


  constructor() { }

  ngOnInit() {
    this.signInForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required])
    });
  }


  public signIn() {
    // tslint:disable-next-line:forin
    for (const i in this.signInForm.controls) {
      this.signInForm.controls[i].markAsDirty();
      this.signInForm.controls[i].updateValueAndValidity();
    }
    if (this.signInForm.invalid) {
      return;
    }
    this.emitterSignIn.emit(this.signInForm.value);
  }

  forgotPassword() {
    this.emitterForgotPsw.emit('ok');
  }
}
