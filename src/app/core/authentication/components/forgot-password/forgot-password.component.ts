import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import * as ServicesStatusActions from '@state/services-status/services-status.actions';
import {ServicesSignatures} from '@shared/enums/services-signatures.enum';
import {select, Store} from '@ngrx/store';
import * as fromAuth from '@state/auth';
import * as AuthActions from '@state/auth/auth.actions';
import * as ServicesStatusSelectors from '@state/services-status/services-status.selector';
import {MessageType} from '@core/enums/message-type';
import {Subscription} from 'rxjs';
import {ServicesStatus} from '@state/services-status/services-status.interface';
import {ToastService} from '@core/services/toast.service';
import {NzModalRef} from 'ng-zorro-antd';

@Component({
  selector: 'fact-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {


  public forgotPswForm: FormGroup;
  public forgotStatusSubscription: Subscription;
  public forgotPasswordStatus: ServicesStatus;
  private loaded: boolean;


  constructor(
    private store: Store<fromAuth.State>,
    private toastService: ToastService,
    private modal: NzModalRef,
  ) {
    this.loaded = false;
    this.forgotStatusSubscription = this.store.pipe(
      select(ServicesStatusSelectors.getServiceStateById, { signature: ServicesSignatures.FORGOT_PSW })
    ).subscribe(status => {
      this.forgotPasswordStatus = status || { loading: false, loaded: false, error: '', signature: ServicesSignatures.FORGOT_PSW };
      if (this.forgotPasswordStatus.loaded && !this.loaded) {
        this.loaded = true;
        this.toastService.showMessage(MessageType.SUCCESS, 'successSendEmailForgotPsw');
        this.store.dispatch(ServicesStatusActions.RemoveStatusAction({ signature: ServicesSignatures.CLIENTS_ADD }));
        this.modal.destroy();
      }
    });
  }

  ngOnInit() {
    this.forgotPswForm = new FormGroup({ email: new FormControl('', [Validators.required])});
  }

  forgotPsw() {
    // tslint:disable-next-line:forin
    for (const i in this.forgotPswForm.controls) {
      this.forgotPswForm.controls[i].markAsDirty();
      this.forgotPswForm.controls[i].updateValueAndValidity();
    }
    if (this.forgotPswForm.invalid) {
      return;
    }
    this.store.dispatch(ServicesStatusActions.ShowLoadingAction({ signature: ServicesSignatures.FORGOT_PSW }));
    this.store.dispatch(AuthActions.ForgotPasswordAction( {data: this.forgotPswForm.value, signature: ServicesSignatures.FORGOT_PSW}));
  }
}
