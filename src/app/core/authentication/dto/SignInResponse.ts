export interface SignInResponse {
  name: string;
  rol: string;
  token: string;
  change: boolean;
}
