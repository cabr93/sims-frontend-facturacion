/**
 * Estructura del request del servicio de inicio de sesión
 */
export interface SignInRequest {
  email: string;
  password: string;
}
