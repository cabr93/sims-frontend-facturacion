export enum MessageType {
  SUCCESS,
  ERROR,
  INFO,
  WARNING,
  LOADING
}
