import { NgModule } from '@angular/core';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {HttpInterceptorsService} from '@core/interceptors/http-interceptors.service';
import { NotFoundComponent } from './pages/not-found/not-found.component';



@NgModule({
  declarations: [NotFoundComponent],
  imports: [],
  providers: [
    {
    provide: HTTP_INTERCEPTORS,
    useClass: HttpInterceptorsService,
    multi: true
    }
  ]
})
export class CoreModule { }
