import {Injectable} from '@angular/core';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {AuthService} from '@core/services/auth.service';
import {ToastService} from '@core/services/toast.service';
import {catchError} from 'rxjs/operators';
import {MessageType} from '@core/enums/message-type';
import {Observable, throwError} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpInterceptorsService implements HttpInterceptor {

  constructor(
    private authService: AuthService,
    private toastService: ToastService
  ) { }

  private static addAuthenticationToken(request: HttpRequest<any>): HttpRequest<any> {
    if (request.url.includes('assets')) {
      return request;
    }
    const token = AuthService.getToken();
    if (!token) {
     return request;
    }

    return request.clone({
    setHeaders: {
      Authorization: `Bearer ${token}`
    }
    });
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(HttpInterceptorsService.addAuthenticationToken(req)).pipe(
      catchError( (err: HttpErrorResponse) => {
        if ( err instanceof HttpErrorResponse && err.status === 401 && !!AuthService.getToken()) {
          this.authService.logout();
          this.toastService.showMessage(MessageType.ERROR, 'expiredToken');
          return throwError(err);
        }
        return throwError(err);
      })
    );
  }

}
