import { Injectable } from '@angular/core';
import {CanLoad, Route, UrlSegment, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router, CanActivate} from '@angular/router';
import { Observable } from 'rxjs';
import {AuthService} from '@core/services/auth.service';
import {TranslateService} from '@ngx-translate/core';
import {ItemMenu} from '@shared/interfaces/item-menu.interface';
import { ActivatedRoute } from '@angular/router';
import {Paths} from '@shared/enums/paths.enum';

@Injectable({
  providedIn: 'root'
})
export class RolesGuard implements  CanActivate {
  constructor(
    private router: Router,
    private translateService: TranslateService,
    private activatedRoute: ActivatedRoute
  ) { }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    const itemsMenu: Array<ItemMenu> = this.translateService.instant('mainMenu');
    let path: string;
    const user = AuthService.currentUser;
    if (document.location.pathname === '/' || document.location.pathname === '/main' || document.location.pathname.includes('sign-in')) {
      return true;
    } else {
      path = next.url[0].path;
    }
    const load = itemsMenu.find( r => r.path === path);
    if (!!load) {
      const isNew = AuthService.isNewPsw();
      if (isNew) {
        this.router.navigate([Paths.MAIN, Paths.CHANGE]);
      }
      return load.role ===  user.rol;
    }
    return false;
  }

}
