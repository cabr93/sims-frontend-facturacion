import { Injectable } from '@angular/core';
import {CanActivate, CanLoad, Route, UrlSegment, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router} from '@angular/router';
import { Observable } from 'rxjs';
import {AuthService} from '@core/services/auth.service';
import {Paths} from '@shared/enums/paths.enum';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate, CanLoad {

  constructor(
    private router: Router
  ) {
  }
  canActivate(): boolean {
    const isLogin = AuthService.isLogin();
    if (!isLogin) {
      this.router.navigate([Paths.AUTH, Paths.SIGN_IN]);
    }
    return isLogin;
  }
  canLoad(): boolean {
    const isLogin = AuthService.isLogin();
    if (!isLogin) {
      this.router.navigate([Paths.AUTH, Paths.SIGN_IN]);
    }
    return isLogin;
  }
}

