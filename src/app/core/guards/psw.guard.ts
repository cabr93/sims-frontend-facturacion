import { Injectable } from '@angular/core';
import {CanActivate, CanLoad, Route, UrlSegment, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router} from '@angular/router';
import {AuthService} from '@core/services/auth.service';
import {Paths} from '@shared/enums/paths.enum';

@Injectable({
  providedIn: 'root'
})
export class PswGuard implements CanActivate, CanLoad {
  constructor(
    private router: Router
  ) { }
  canActivate(): boolean {
    const isNew = AuthService.isNewPsw();
    if (isNew) {
      this.router.navigate([Paths.MAIN, Paths.CHANGE]);
    }
    return !isNew;
  }
  canLoad(): boolean {
    const isNew = AuthService.isNewPsw();
    console.log(isNew);
    if (isNew) {
      this.router.navigate([Paths.MAIN, Paths.CHANGE]);
    }
    return !isNew;
  }
}
