/**
 * Modelo del usuario que inicío sesión
 */

export class User {
  constructor(
    public userName: string,
    public rol: string,
    public change?: boolean,
    // tslint:disable-next-line:variable-name
    public _id?: string,
    public email?: string,
    public position?: string,
    public docId?: string
  ) {}
}
