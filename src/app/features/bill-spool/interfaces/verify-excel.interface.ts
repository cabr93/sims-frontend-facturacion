export interface VerifyExcelInterface {
  company: string;
  client: string;
  idBill: string;
  prefix: string;
}
