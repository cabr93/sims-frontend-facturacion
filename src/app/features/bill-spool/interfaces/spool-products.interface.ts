export interface SpoolProducts {
  amount: number;
  tax: number;
  pricePerUnit: number;
  subtotal: number;
  subtotalTax: number;
  total: number;
  message: string;
}
