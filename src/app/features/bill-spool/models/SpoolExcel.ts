import {SpoolProducts} from '@app/features/bill-spool/interfaces/spool-products.interface';

export class SpoolExcel {
  constructor(
    public consecutive: string,
    public payment: string,
    public currency: string,
    public commercialNameCompany: string,
    public commercialNameClient: string,
    public products: Array<SpoolProducts>,
    public prefix: string,
    public total: number,
    public tax: number,
  ) {}
}
