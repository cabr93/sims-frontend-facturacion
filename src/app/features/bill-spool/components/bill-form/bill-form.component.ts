import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import * as XLSX from 'xlsx';
import { UploadXHRArgs } from 'ng-zorro-antd';
import {ExcelData} from '@app/features/bill-spool/interfaces/excel-data.interface';
import {select, Store} from '@ngrx/store';
import * as ServicesStatusSelectors from '@state/services-status/services-status.selector';
import {ServicesSignatures} from '@shared/enums/services-signatures.enum';
import {ServicesStatus} from '@state/services-status/services-status.interface';
import {Subscription} from 'rxjs';
import * as fromBills from '@state/bill';

@Component({
  selector: 'fact-bill-form',
  templateUrl: './bill-form.component.html',
  styleUrls: ['./bill-form.component.scss']
})
export class BillFormComponent implements OnInit {

  @Input()
  loading: boolean;

  @Output()
  emitterDocument: EventEmitter<ExcelData> = new EventEmitter<ExcelData>();

  public verifySpoolSend: ServicesStatus;
  public verifySpoolSentSubscription: Subscription;
  private fileUploaded: File;
  private storeData: any;
  private worksheet: any;
  public name: string;
  public uploadingFile: boolean;
  public comments: string;

  constructor(
    private store: Store<fromBills.State>,
  ) {
    this.verifySpoolSentSubscription = this.store.pipe(
      select(ServicesStatusSelectors.getServiceStateById, { signature: ServicesSignatures.SEND_SPOOL })
    ).subscribe(status => {
      this.verifySpoolSend = status || { loading: false, loaded: false, error: '', signature: ServicesSignatures.VERIFY_EXCEL };
      if (this.verifySpoolSend.loaded) {
        this.uploadingFile = false;

      }
    });
  }

  ngOnInit() {
    this.uploadingFile = false;
  }


  customUploadReq = (item: UploadXHRArgs) => {
    this.uploadingFile = true;
    this.fileUploaded = item.file as any;
    this.name = item.file.name;
    this.readExcel();
  }


  readExcel() {
    const readFile = new FileReader();
    readFile.onload = (e) => {
      this.storeData = readFile.result;
      const data = new Uint8Array(this.storeData);
      const arr = [];
      // tslint:disable-next-line: triple-equals
      for (let i = 0; i != data.length; ++i) {
        arr[i] = String.fromCharCode(data[i]);
      }
      const bstr = arr.join('');
      const workbook = XLSX.read(bstr, { type: 'binary' });
      // tslint:disable-next-line: variable-name
      const first_sheet_name = workbook.SheetNames[0];
      this.worksheet = workbook.Sheets[first_sheet_name];
      let N = 0;
      for (let i = 1; i < Object.keys(this.worksheet).length ; i++) {
        const element = 'A' + i;
        const value = this.worksheet[element];
        if (value === undefined) {
          N = i ;
          break;
        }
      }
    };
    readFile.readAsArrayBuffer(this.fileUploaded);
  }

  uploadData() {
    this.emitterDocument.emit({data: this.worksheet, message: this.comments});
  }


}
