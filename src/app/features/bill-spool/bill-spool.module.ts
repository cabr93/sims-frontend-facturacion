import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BillSpoolRoutingModule } from './bill-spool-routing.module';
import { BillComponent } from './containers/bill/bill.component';
import {SharedModule} from '@shared/shared.module';
import { BillFormComponent } from './components/bill-form/bill-form.component';
import {FormsModule} from '@angular/forms';
import {StoreModule} from '@ngrx/store';
import * as billsReducer from '@state/bill/bills.reducer';
import {EffectsModule} from '@ngrx/effects';
import {BillsEffects} from '@state/bill/bills.effects';
import { BillSpoolFormComponent } from './containers/bill-spool-form/bill-spool-form.component';


@NgModule({
  declarations: [BillComponent, BillFormComponent, BillSpoolFormComponent],
  entryComponents: [
    BillSpoolFormComponent
  ],
  imports: [
    CommonModule,
    BillSpoolRoutingModule,
    SharedModule,
    StoreModule.forFeature('bills', billsReducer.reducer),
    EffectsModule.forFeature([ BillsEffects ]),
    FormsModule
  ]
})
export class BillSpoolModule { }
