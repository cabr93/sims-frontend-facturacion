import {Clients} from '@app/features/client/models/Clients';
import {Company} from '@app/features/companies/models/Company';

export interface VerifyExcelResponse {
  client: Clients;
  company: Company;
}
