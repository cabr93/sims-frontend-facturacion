import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {SpoolExcel} from '@app/features/bill-spool/models/SpoolExcel';
import {select, Store} from '@ngrx/store';
import * as ServicesStatusSelectors from '@state/services-status/services-status.selector';
import {ServicesSignatures} from '@shared/enums/services-signatures.enum';
import * as fromBills from '@state/bill';
import {Clients} from '@app/features/client/models/Clients';
import {Company} from '@app/features/companies/models/Company';
import {Subscription} from 'rxjs';
import {ServicesStatus} from '@state/services-status/services-status.interface';
import {ItemSelect} from '@shared/interfaces/item-select.interface';
import * as ServicesStatusActions from '@state/services-status/services-status.actions';
import {MessageType} from '@core/enums/message-type';
import {NzModalRef} from 'ng-zorro-antd';
import {ToastService} from '@core/services/toast.service';
import * as BillsActions from '@state/bill/bills.actions';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'fact-bill-spool-form',
  templateUrl: './bill-spool-form.component.html',
  styleUrls: ['./bill-spool-form.component.scss']
})
export class BillSpoolFormComponent implements OnInit, OnDestroy {

  @Input()
  excelData: SpoolExcel;

  public loaded: boolean;
  private sendLoaded: boolean;

  public spoolPayment: Array<ItemSelect>;
  public client: Clients;
  public company: Company;
  public showDetail: boolean;
  public showDiscount: boolean;
  public verifyExcelStatusSubscription: Subscription;
  public sendBillStatusSubscription: Subscription;

  public mapOfExpandData: { [key: string]: boolean } = {};

  public sendBillStatus: ServicesStatus;

  public billSpoolForm: FormGroup;

  public verifyExcelStatus: ServicesStatus;

  constructor(
    private store: Store<fromBills.State>,
    private modal: NzModalRef,
    private translateService: TranslateService,
    private toastService: ToastService
  ) {
    this.loaded = false;
    this.sendLoaded = false;
    this.verifyExcelStatusSubscription = this.store.pipe(
      select(ServicesStatusSelectors.getServiceStateById, { signature: ServicesSignatures.VERIFY_EXCEL })
    ).subscribe((status: ServicesStatus) => {
      this.verifyExcelStatus = status || { loading: false, loaded: false, error: '', signature: ServicesSignatures.VERIFY_EXCEL };
      this.loaded = status.loaded || false;
      console.log(this.verifyExcelStatus);
      if ( !this.verifyExcelStatus.loaded && !this.verifyExcelStatus.loading) {
        this.modal.destroy();
      }
    });
    this.sendBillStatusSubscription = this.store.pipe(
      select(ServicesStatusSelectors.getServiceStateById, { signature: ServicesSignatures.SEND_SPOOL })
    ).subscribe((status: ServicesStatus) => {
      this.sendBillStatus = status || { loading: false, loaded: false, error: '', signature: ServicesSignatures.SEND_SPOOL };
      if (this.sendBillStatus.loaded && !this.sendLoaded) {
        this.sendLoaded = true;
        this.toastService.showMessage(MessageType.SUCCESS, 'successSpoolSend');
        this.store.dispatch(ServicesStatusActions.RemoveStatusAction({ signature: ServicesSignatures.SEND_SPOOL }));
        this.modal.destroy();
      }
    });
  }


  ngOnInit() {
    this.translateService.get( 'SpoolPaymentType').subscribe(items => this.spoolPayment = items);
    this.showDetail = false;
    this.showDiscount = false;
    this.billSpoolForm = new FormGroup({
      paymentType: new FormControl('30', Validators.required),
      paymentDeadline: new FormControl('' ),
      trm: new FormControl('' , Validators.required),
      discount: new FormControl(2, [Validators.required, this.discountValidator.bind(this)]),
      valueDiscount: new FormControl('', ),
      companyCity: new FormControl( '', Validators.required)
    }, this.trmValidator.bind(this));
    this.store.pipe( select( fromBills.getClientCompany)).subscribe(
      (data: any) => {
        this.client = data[0];
        this.company = data[1];
        if (this.company) {
          this.billSpoolForm.controls.companyCity.setValue(this.company.mainLocation);
        }
      }
    );
  }


  ngOnDestroy() {
    this.verifyExcelStatusSubscription.unsubscribe();
    this.sendBillStatusSubscription.unsubscribe();
  }

  public discountValidator( control: FormControl): {[s: string]: boolean} {
    if (this.billSpoolForm) {
      if (control.value === 1) {
        this.showDiscount = true;
        this.billSpoolForm.controls.valueDiscount.setValidators([
          Validators.required,
          Validators.pattern('^[0-9]+\.?[0-9]*$'),
          Validators.min(0),
          Validators.max(100),
        ]);
      } else {
        this.showDiscount = false;
        this.billSpoolForm.controls.valueDiscount.setValue('');
        this.billSpoolForm.controls.valueDiscount.setValidators([]);
      }
    }
    return null;
  }
  public trmValidator( control: FormControl): {[s: string]: boolean} {
    if (this.excelData && this.billSpoolForm) {
      if (this.excelData.currency === 'COP') {
        this.billSpoolForm.controls.trm.setValidators([]);
      } else {
        this.billSpoolForm.controls.trm.setValidators([
            Validators.required
          ]);
      }
    }
    return null;
  }

  sendSpool() {
    // tslint:disable-next-line:forin
    for (const i in this.billSpoolForm.controls) {
      this.billSpoolForm.controls[i].markAsDirty();
      this.billSpoolForm.controls[i].updateValueAndValidity();
    }

    if (this.billSpoolForm.invalid) {
      return;
    }
    this.store.dispatch(ServicesStatusActions.ShowLoadingAction({ signature: ServicesSignatures.SEND_SPOOL }));
    this.store.dispatch(BillsActions.SendSpoolAction({client: this.client, company: this.company, form: this.billSpoolForm.value, excel: this.excelData}));
  }
}
