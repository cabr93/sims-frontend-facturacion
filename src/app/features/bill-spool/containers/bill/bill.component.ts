import {Component, OnDestroy, OnInit} from '@angular/core';
import {ExcelData} from '@app/features/bill-spool/interfaces/excel-data.interface';
import {select, Store} from '@ngrx/store';
import * as fromBills from '@state/bill';
import * as BillsActions from '@state/bill/bills.actions';
import {ServicesSignatures} from '@shared/enums/services-signatures.enum';
import * as ServicesStatusActions from '@state/services-status/services-status.actions';
import * as ServicesStatusSelectors from '@state/services-status/services-status.selector';
import {Subscription} from 'rxjs';
import {ServicesStatus} from '@state/services-status/services-status.interface';
import {NzModalService} from 'ng-zorro-antd';
import {BillSpoolFormComponent} from '@app/features/bill-spool/containers/bill-spool-form/bill-spool-form.component';
import {CompaniesSpool} from '@app/features/bill-spool/enums/companies-spool.enum';
import {SpoolExcel} from '@app/features/bill-spool/models/SpoolExcel';

@Component({
  selector: 'fact-bill',
  templateUrl: './bill.component.html',
  styleUrls: ['./bill.component.scss']
})
export class BillComponent implements OnInit, OnDestroy {

  public verifyExcelStatusSubscription: Subscription;
  public verifyExcelStatus: ServicesStatus;
  public excel: any;
  public spool: SpoolExcel;
  public showModal: boolean;

  constructor(
    private store: Store<fromBills.State>,
    private modalService: NzModalService
  ) {
    this.showModal = false;
    this.verifyExcelStatusSubscription = this.store.pipe(
      select(ServicesStatusSelectors.getServiceStateById, { signature: ServicesSignatures.VERIFY_EXCEL })
    ).subscribe(status => {
      this.verifyExcelStatus = status || { loading: false, loaded: false, error: '', signature: ServicesSignatures.VERIFY_EXCEL };
    });

  }

  ngOnInit() {
  }


  ngOnDestroy() {
    this.verifyExcelStatusSubscription.unsubscribe();
    this.modalService.closeAll();
  }

  readExcel(data: ExcelData) {
    this.excel = data;
    this.store.dispatch(ServicesStatusActions.ShowLoadingAction( {signature: ServicesSignatures.VERIFY_EXCEL}));
    switch (data.data.E2.w) {
      case CompaniesSpool.TECH:
        this.excelTech();
        break;
      case CompaniesSpool.LTDA:
        this.excelLER();
        break;
      case CompaniesSpool.EAGLE:
        this.excelLER();
        break;
      case CompaniesSpool.RENTING:
        this.excelLER();
        break;
    }
  }
  private excelTech() {
    const data = this.excel.data;
    this.spool = {
      prefix: data.A2.w,
      consecutive: data.C2.w,
      payment: data.O2.w,
      currency: data.P2 === undefined ? 'COP' : 'USD',
      commercialNameCompany: data.E2.w,
      commercialNameClient: data.G2.w,
      products: [],
      total : 0,
      tax : 0
    };
    let flag = true;
    let cont = 0;
    while (flag) {
      cont++;
      flag = data.hasOwnProperty(`A${cont}`);
    }
    for (let i = 2; i < cont; i++) {
      this.spool.products.push({
        amount: data[`X${i}`].w,
        tax: data[`Y${i}`].w,
        pricePerUnit: data[`Z${i}`].w,
        subtotal: data[`AC${i}`].w,
        subtotalTax: data[`AC${i}`].w * data[`Y${i}`].w,
        total:  data[`AC${i}`].w * (+data[`Y${i}`].w + 1),
        message: data[`AI${i}`].w,
      });
      this.spool.tax += data[`AC${i}`].w * data[`Y${i}`].w;
      this.spool.total += +data[`AC${i}`].w;
    }
    this.verifyData();
  }

  private excelLER() {
    const data = this.excel.data;
    this.spool = {
      prefix: data.A2.w,
      consecutive: data.C2.w,
      payment: data.O2.w,
      currency: data.P2 === undefined ? 'COP' : 'USD',
      commercialNameCompany: data.E2.w,
      commercialNameClient: data.G2.w,
      products: [],
      total : 0,
      tax : 0
    };
    let flag = true;
    let cont = 0;
    while (flag) {
      cont++;
      flag = data.hasOwnProperty(`A${cont}`);
    }
    for (let i = 2; i < cont; i++) {
      this.spool.products.push({
        amount: data[`X${i}`].w,
        tax: data[`Y${i}`].w,
        pricePerUnit: data[`Z${i}`].w,
        subtotal: data[`AB${i}`].w,
        subtotalTax: data[`AB${i}`].w * data[`Y${i}`].w,
        total:  data[`AB${i}`].w * (+data[`Y${i}`].w + 1),
        message: data[`AH${i}`] ? data[`AH${i}`].w : '',
      });
      this.spool.tax += data[`AB${i}`].w * data[`Y${i}`].w;
      this.spool.total += +data[`AB${i}`].w;
    }
    this.verifyData();
  }

  private verifyData() {
    this.store.dispatch(BillsActions.VerifyExcelAction({
      data: {
        company: this.spool.commercialNameCompany ,
        client: this.spool.commercialNameClient,
        idBill: this.spool.consecutive,
        prefix: this.spool.prefix
      },
      signature: ServicesSignatures.VERIFY_EXCEL
    }));
    this.modalService.create({
      nzContent: BillSpoolFormComponent,
      nzClassName: 'app-add-edit-form',
      nzWidth: 900,
      nzComponentParams: {
        excelData: this.spool,
      }
    });
  }

}
