import { Injectable } from '@angular/core';
import {HttpInvokeService} from '@core/services/http-invoke.service';
import {ExcelData} from '@app/features/bill-spool/interfaces/excel-data.interface';
import {Observable} from 'rxjs';
import {ServicesSignatures} from '@shared/enums/services-signatures.enum';
import {VerifyExcelResponse} from '@app/features/bill-spool/dto/verifyExcelResponse';

@Injectable({
  providedIn: 'root'
})
export class BillsService {

  constructor(
    private httpInvokeService: HttpInvokeService
  ) { }

  public verifyExcel( data: any): Observable<VerifyExcelResponse> {
    return this.httpInvokeService
      .post<any, VerifyExcelResponse>('/spoolExcel', data, ServicesSignatures.VERIFY_EXCEL, true);
  }

  public sendSpool( data: any): Observable<any> {
    return this.httpInvokeService
      .post<any, any>('/spool', data, ServicesSignatures.SEND_SPOOL, true);
  }
}
