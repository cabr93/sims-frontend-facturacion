import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {User} from '@core/models/User';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ItemSelect} from '@shared/interfaces/item-select.interface';
import {TranslateService} from '@ngx-translate/core';
import * as ServicesStatusActions from '@state/services-status/services-status.actions';
import {ServicesSignatures} from '@shared/enums/services-signatures.enum';
import * as UsersActions from '@state/users/users.actions';
import {select, Store} from '@ngrx/store';
import * as fromUsers from '@state/users';
import * as ServicesStatusSelectors from '@state/services-status/services-status.selector';
import {MessageType} from '@core/enums/message-type';
import {Subscription} from 'rxjs';
import {ServicesStatus} from '@state/services-status/services-status.interface';
import {ToastService} from '@core/services/toast.service';
import {NzModalRef} from 'ng-zorro-antd';

@Component({
  selector: 'fact-add-edit-user-form',
  templateUrl: './add-edit-user-form.component.html',
  styleUrls: ['./add-edit-user-form.component.scss']
})
export class AddEditUserFormComponent implements OnInit, OnDestroy {
  @Input()
  editingUser: User;

  public addEditUserForm: FormGroup;

  public editing: boolean;
  public roles: Array<ItemSelect>;
  private loaded: boolean;
  public nameFlag: boolean;

  public UserStatusSubscription: Subscription;

  public addOrEditUserStatus: ServicesStatus;


  constructor(
    private translateService: TranslateService,
    private store: Store<fromUsers.State>,
    private toastService: ToastService,
    private modal: NzModalRef
  ) {
    this.loaded = false;
    this.UserStatusSubscription = this.store.pipe(
      select(ServicesStatusSelectors.getServiceStateById, { signature: ServicesSignatures.USERS_ADD })
    ).subscribe(status => {
      this.addOrEditUserStatus = status || { loading: false, loaded: false, error: '', signature: ServicesSignatures.USERS_ADD };
      if (this.addOrEditUserStatus.loaded && !this.loaded) {
        this.loaded = true;
        const key = this.editing ? 'successUserUpdated' : 'successUserSaved';
        this.toastService.showMessage(MessageType.SUCCESS, key);
        this.store.dispatch(ServicesStatusActions.RemoveStatusAction({ signature: ServicesSignatures.USERS_ADD }));
        this.modal.destroy();
      }
    });
  }

  ngOnInit() {
    this.editing = !!this.editingUser;
    this.translateService.get( 'UserRoles').subscribe(items => this.roles = items);

    this.addEditUserForm = new FormGroup({
      userName: new FormControl(
        this.editing ? this.editingUser.userName : null,
        [Validators.required]
      ),
      rol: new FormControl(
        this.editing ? this.editingUser.rol : null,
        [Validators.required]
      ),
      email: new FormControl(
        this.editing ? this.editingUser.email : null,
        [Validators.required, Validators.email]
      ),
      position: new FormControl(
        this.editing ? this.editingUser.position : null,
        [Validators.required]
      ),
      docId: new FormControl(
        this.editing ? this.editingUser.docId : null,
        [Validators.required]
      )
    });
  }
  ngOnDestroy() {
    this.UserStatusSubscription.unsubscribe();
  }

  saveUser() {
    // tslint:disable-next-line:forin
    for (const i in this.addEditUserForm.controls) {
      this.addEditUserForm.controls[i].markAsDirty();
      this.addEditUserForm.controls[i].updateValueAndValidity();
    }
    if (this.addEditUserForm.invalid) {
      return;
    }
    this.store.dispatch(ServicesStatusActions.ShowLoadingAction({ signature: ServicesSignatures.USERS_ADD }));
    if (this.editing) {
      this.store.dispatch(UsersActions.EditUsersAction({users: {...this.addEditUserForm.value, _id: this.editingUser._id }}));
    } else {
      // tslint:disable-next-line:max-line-length
      this.store.dispatch(UsersActions.CreateUsersAction({users: this.addEditUserForm.value}));
    }
  }
}
