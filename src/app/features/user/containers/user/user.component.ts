import { Component, OnInit } from '@angular/core';
import {select, Store} from '@ngrx/store';
import * as fromUsers from '@state/users';
import {NzModalService} from 'ng-zorro-antd';
import * as ServicesStatusSelectors from '@state/services-status/services-status.selector';
import {ServicesSignatures} from '@shared/enums/services-signatures.enum';
import {Observable} from 'rxjs';
import {ServicesStatus} from '@state/services-status/services-status.interface';
import {User} from '@core/models/User';
import * as ServicesStatusActions from '@state/services-status/services-status.actions';
import * as UsersAction from '@state/users/users.actions';
import {AddEditUserFormComponent} from '@app/features/user/containers/add-edit-user-form/add-edit-user-form.component';
import * as ClientsActions from '@state/clients/clients.actions';

@Component({
  selector: 'fact-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  public users: Observable<User[]>;

  public querySearch: string;

  public addOrEditUsersStatus: Observable<ServicesStatus>;

  constructor(
    private store: Store<fromUsers.State>,
    private modalService: NzModalService
  ) {
    this.users = this.store.pipe(select(fromUsers.getUsers));
    this.addOrEditUsersStatus = this.store.pipe(
      select(ServicesStatusSelectors.getServiceStateById, { signature: ServicesSignatures.USERS })
    );
  }

  ngOnInit() {
    this.store.dispatch(ServicesStatusActions.ShowLoadingAction({ signature: ServicesSignatures.USERS }));
    this.store.dispatch(UsersAction.GetUsersAction({ signature: ServicesSignatures.USERS }));
  }

  addOrEditUser( user: User = null) {
    this.modalService.create({
      nzContent: AddEditUserFormComponent,
      nzClassName: 'app-add-edit-form',
      nzWidth: 600,
      nzComponentParams: {
        editingUser: user
      }
    });
  }
  // tslint:disable-next-line:variable-name
  deleteUser( _id: string) {
    this.store.dispatch(UsersAction.DeleteUsersAction({ _id, signature: ServicesSignatures.USERS }));
  }

}
