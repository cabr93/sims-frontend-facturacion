import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {ServicesSignatures} from '@shared/enums/services-signatures.enum';
import {HttpInvokeService} from '@core/services/http-invoke.service';
import {User} from '@core/models/User';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private httpInvokeService: HttpInvokeService
  ) { }

  public getUsers(): Observable<User[]> {
    return this.httpInvokeService
      .get<User[]>('/users', ServicesSignatures.USERS , false);
  }

  public newUser(user: User): Observable<any> {
    return this.httpInvokeService
      .post<User, any>('/users', user, ServicesSignatures.USERS , true);
  }
  public editUser(user: User): Observable<any> {
    return this.httpInvokeService
      .put<User, any>('/users', user, ServicesSignatures.USERS , true);
  }
  public deleteUser( id: string): Observable<any> {
    return this.httpInvokeService
      .delete<string, any>(`/users?id=${id}`, ServicesSignatures.USERS , true);
  }
}
