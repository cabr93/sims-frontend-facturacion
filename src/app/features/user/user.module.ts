import { NgModule } from '@angular/core';

import { UserRoutingModule } from './user-routing.module';
import { UserComponent } from './containers/user/user.component';
import { AddEditUserFormComponent } from './containers/add-edit-user-form/add-edit-user-form.component';
import {SharedModule} from '@shared/shared.module';
import { TableUsersComponent } from './components/table-users/table-users.component';
import {FormsModule} from '@angular/forms';
import {StoreModule} from '@ngrx/store';
import * as usersReducers from '@state/users/users.reducer';
import {EffectsModule} from '@ngrx/effects';
import {UsersEffects} from '@state/users/users.effects';


@NgModule({
  declarations: [
    UserComponent,
    AddEditUserFormComponent,
    TableUsersComponent
  ],
  entryComponents: [
    AddEditUserFormComponent
  ],
  imports: [
    UserRoutingModule,
    SharedModule,
    StoreModule.forFeature('users', usersReducers.reducer),
    EffectsModule.forFeature([ UsersEffects ]),
    FormsModule
  ]
})
export class UserModule { }
