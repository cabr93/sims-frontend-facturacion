import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FactSearchService} from '@shared/enums/fact-search-service.enum';
import {ServicesStatus} from '@state/services-status/services-status.interface';
import {Clients} from '@app/features/client/models/Clients';
import {User} from '@core/models/User';

@Component({
  selector: 'fact-table-users',
  templateUrl: './table-users.component.html',
  styleUrls: ['./table-users.component.scss']
})
export class TableUsersComponent implements OnInit {

  get servicesTypes() {
    return FactSearchService;
  }

  /**
   * Propiedad que recibe el estado en el que está el consumo del servicio de obtener dispositivos.
   */
  @Input()
  serviceStatus: ServicesStatus;

  @Input()
  users: User[];

  @Output()
  deleteUserEmitter: EventEmitter<string> = new EventEmitter<string>();

  @Output()
  editUserEmitter: EventEmitter<Clients> = new EventEmitter<Clients>();
  constructor() { }

  @Input()
  querySearch: string;

  ngOnInit() {
  }

}
