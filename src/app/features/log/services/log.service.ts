import { Injectable } from '@angular/core';
import {HttpInvokeService} from '@core/services/http-invoke.service';
import {Observable} from 'rxjs';
import {ServicesSignatures} from '@shared/enums/services-signatures.enum';
import {Logs} from '@app/features/log/models/Logs';

@Injectable({
  providedIn: 'root'
})
export class LogService {

  constructor(
    private httpInvokeService: HttpInvokeService
  ) { }

  public getLogs(): Observable<Logs[]> {
    return this.httpInvokeService
      .get<Logs[]>('/logs', ServicesSignatures.LOGS , false);
  }

}
