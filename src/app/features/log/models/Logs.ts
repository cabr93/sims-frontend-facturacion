export class Logs {
  constructor(
    public time: Date,
    public email: string,
    public action: string
  ) { }

  getStringify(): string {
    return `${this.email}${this.action}`;
  }
}
