import { Component, OnInit } from '@angular/core';
import {select, Store} from '@ngrx/store';
import * as fromLogs from '@state/logs';
import {Observable} from 'rxjs';
import {Logs} from '@app/features/log/models/Logs';
import * as LogsActions from '@state/logs/logs.action';
import {ServicesSignatures} from '@shared/enums/services-signatures.enum';

@Component({
  selector: 'fact-log',
  templateUrl: './log.component.html',
  styleUrls: ['./log.component.scss']
})
export class LogComponent implements OnInit {

  public querySearch: string;

  public logs: Observable<Logs[]>;


  constructor(
    private store: Store<fromLogs.State>,
  ) {
    this.logs = this.store.pipe(select(fromLogs.getLogs));
  }

  ngOnInit() {
    this.store.dispatch(LogsActions.GetLogsAction({ signature: ServicesSignatures.LOGS }));
  }

}
