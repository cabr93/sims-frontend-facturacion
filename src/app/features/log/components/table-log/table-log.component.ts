import {Component, Input, OnInit} from '@angular/core';
import {Logs} from '@app/features/log/models/Logs';
import {FactSearchService} from '@shared/enums/fact-search-service.enum';

@Component({
  selector: 'fact-table-log',
  templateUrl: './table-log.component.html',
  styleUrls: ['./table-log.component.scss']
})
export class TableLogComponent implements OnInit {

  get servicesTypes() {
    return FactSearchService;
  }

  @Input()
  logs: Logs[];


  @Input()
  querySearch: string;

  constructor() { }

  ngOnInit() {
  }

}
