import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LogRoutingModule } from './log-routing.module';
import { LogComponent } from './containers/log/log.component';
import {SharedModule} from '@shared/shared.module';
import { TableLogComponent } from './components/table-log/table-log.component';
import {FormsModule} from '@angular/forms';
import {StoreModule} from '@ngrx/store';
import * as logsReducer from '@state/logs/logs.reducer';
import {EffectsModule} from '@ngrx/effects';
import {LogsEffects} from '@state/logs/logs.effects';


@NgModule({
  declarations: [LogComponent, TableLogComponent],
  imports: [
    LogRoutingModule,
    SharedModule,
    FormsModule,
    StoreModule.forFeature('logs', logsReducer.reducer),
    EffectsModule.forFeature([ LogsEffects ])
  ]
})
export class LogModule { }
