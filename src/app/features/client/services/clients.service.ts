import { Injectable } from '@angular/core';
import {HttpInvokeService} from '@core/services/http-invoke.service';
import {Observable} from 'rxjs';
import {Clients} from '@app/features/client/models/Clients';
import {ServicesSignatures} from '@shared/enums/services-signatures.enum';

@Injectable({
  providedIn: 'root'
})
export class ClientsService {

  constructor(
    private httpInvokeService: HttpInvokeService
  ) { }

  public getClients(): Observable<Clients[]> {
    return this.httpInvokeService
      .get<Clients[]>('/clients', ServicesSignatures.CLIENTS , false);
  }

  public newClient(client: Clients): Observable<any> {
    return this.httpInvokeService
      .post<Clients, any>('/clients', client, ServicesSignatures.CLIENTS , true);
  }
  public editClient(client: Clients): Observable<any> {
    console.log(client);
    return this.httpInvokeService
      .put<Clients, any>('/clients', client, ServicesSignatures.CLIENTS , true);
  }
  public deleteClient( id: string): Observable<any> {
    return this.httpInvokeService
      .delete<string, any>(`/clients?id=${id}`, ServicesSignatures.CLIENTS , true);
  }
}

