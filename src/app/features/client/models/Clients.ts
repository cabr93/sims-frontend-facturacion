export class Clients {
  constructor(
    // tslint:disable-next-line:variable-name
    public _id: string,
    public typeId: string,
    public typeDocId: string,
    public numberDocId: string,
    public verificationCode: string,
    public taxRegime: string,
    public fiscalResponsibility: string,
    public operationType: string,
    public commercialName: string,
    public socialReason: string,
    public firstName: string,
    public middleName: string,
    public firstLastName: string,
    public secondLastName: string,
    public email: string,
    public phone: string,
    public address: string,
    public city: string,
    public department: string,
    public municipality: string,
    public country: string,
    public postalCode: string,
    public observation: string,
  ) { }

  getStringify(): string {
    return `${this.numberDocId}${this.commercialName}${this.firstName}${this.middleName}${this.firstLastName}${this.secondLastName}`;
  }
}
