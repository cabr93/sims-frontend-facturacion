import { NgModule } from '@angular/core';
import * as clientsReducer from '@state/clients/client.reducer';

import { ClientRoutingModule } from './client-routing.module';
import { ClientComponent } from './containers/client/client.component';
import { TableClientComponent } from './components/table-client/table-client.component';
import {SharedModule} from '@shared/shared.module';
import {StoreModule} from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';
import {ClientsEffects} from '@state/clients/clients.effects';
import { FormsModule } from '@angular/forms';
import { AddEditClientFormComponent } from './containers/add-edit-client-form/add-edit-client-form.component';


@NgModule({
  declarations: [
    ClientComponent,
    TableClientComponent,
    AddEditClientFormComponent
  ],
  entryComponents: [
    AddEditClientFormComponent
  ],
  imports: [
    ClientRoutingModule,
    SharedModule,
    StoreModule.forFeature('clients', clientsReducer.reducer),
    EffectsModule.forFeature([ ClientsEffects ]),
    FormsModule
  ]
})
export class ClientModule { }
