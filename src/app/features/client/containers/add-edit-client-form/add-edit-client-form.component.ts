import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {Clients} from '@app/features/client/models/Clients';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {NzModalRef} from 'ng-zorro-antd';
import {TranslateService} from '@ngx-translate/core';
import {ItemSelect} from '@shared/interfaces/item-select.interface';
import {ChangePasswordInterface} from '@app/features/password/interfaces/change-password.interface';
import {Subscription} from 'rxjs';
import {select, Store} from '@ngrx/store';
import * as fromClients from '@state/clients';

import * as ServicesStatusSelectors from '@state/services-status/services-status.selector';
import {ServicesSignatures} from '@shared/enums/services-signatures.enum';
import {ServicesStatus} from '@state/services-status/services-status.interface';
import {ToastService} from '@core/services/toast.service';
import {MessageType} from '@core/enums/message-type';
import * as ServicesStatusActions from '@state/services-status/services-status.actions';
import * as ClientsActions from '@state/clients/clients.actions';


@Component({
  selector: 'fact-add-edit-client-form',
  templateUrl: './add-edit-client-form.component.html',
  styleUrls: ['./add-edit-client-form.component.scss']
})
export class AddEditClientFormComponent implements OnInit, OnDestroy {

  public addEditClientForm: FormGroup;

  public clientTypeof: Array<ItemSelect>;
  public clientTypeDocId: Array<ItemSelect>;
  public fiscalResponsibility: Array<ItemSelect>;
  public operationType: Array<ItemSelect>;
  public cities: Array<ItemSelect>;
  public countries: Array<ItemSelect>;
  public departments: Array<ItemSelect>;

  public editing: boolean;
  private loaded: boolean;
  public nameFlag: boolean;

  /**
   * Suscripción a un Selector del Store que detecta cambios en el estado en el que se encuentra el consumo del servicio
   * de crear o editar un servicio..
   */
  public clientStatusSubscription: Subscription;

  public addOrEditClientStatus: ServicesStatus;


  @Input()
  editingClient: Clients;
  constructor(
    private modal: NzModalRef,
    private translateService: TranslateService,
    private store: Store<fromClients.State>,
    private toastService: ToastService
  ) {
    this.loaded = false;
    this.clientStatusSubscription = this.store.pipe(
      select(ServicesStatusSelectors.getServiceStateById, { signature: ServicesSignatures.CLIENTS_ADD })
    ).subscribe(status => {
      this.addOrEditClientStatus = status || { loading: false, loaded: false, error: '', signature: ServicesSignatures.CLIENTS_ADD };
      console.log(this.addOrEditClientStatus.loaded);
      if (this.addOrEditClientStatus.loaded && !this.loaded) {
        this.loaded = true;
        const key = this.editing ? 'successClientUpdated' : 'successClientSaved';
        this.toastService.showMessage(MessageType.SUCCESS, key);
        this.store.dispatch(ServicesStatusActions.RemoveStatusAction({ signature: ServicesSignatures.CLIENTS_ADD }));
        this.modal.destroy();
      }
    });
  }

  ngOnInit() {
    this.nameFlag = false;
    this.editing = !!this.editingClient;
    this.translateService.get( 'ClientTypeId').subscribe(items => this.clientTypeof = items);
    this.translateService.get( 'ClientTypeDocId').subscribe(items => this.clientTypeDocId = items);
    this.translateService.get( 'ClientFiscalResponsibility').subscribe(items => this.fiscalResponsibility = items);
    this.translateService.get( 'ClientOperationType').subscribe(items => this.operationType = items);
    this.translateService.get( 'ClientCity').subscribe(items => this.cities = items);
    this.translateService.get( 'ClientCountry').subscribe(items => this.countries = items);
    this.translateService.get( 'ClientsDepartment').subscribe(items => this.departments = items);
    this.addEditClientForm = new FormGroup({
      typeId: new FormControl(
        this.editing ? this.editingClient.typeId : null,
        [Validators.required]
      ),
      typeDocId: new FormControl(
        this.editing ? this.editingClient.typeDocId : null,
        [Validators.required, this.typeDocumentValidator.bind(this)]
      ),
      numberDocId: new FormControl(
        this.editing ? this.editingClient.numberDocId : null,
        [ Validators.required, Validators.pattern('^[0-9]*$'), Validators.maxLength(49)]
      ),
      verificationCode: new FormControl(
        this.editing ? this.editingClient.verificationCode : null,
        [ Validators.maxLength(1), Validators.pattern('^[0-9]*$')]
      ),
      taxRegime: new FormControl(
        this.editing ? this.editingClient.taxRegime : null,
        [Validators.required]
      ),
      fiscalResponsibility: new FormControl(
        this.editing ? this.editingClient.fiscalResponsibility : null,
        [Validators.required]
      ),
      operationType: new FormControl(
        this.editing ? this.editingClient.operationType : null,
        [Validators.required]
      ),
      commercialName: new FormControl(
        this.editing ? this.editingClient.commercialName : null,
        [Validators.required, Validators.maxLength(299)]
      ),
      socialReason: new FormControl(
        this.editing ? this.editingClient.socialReason : null,
        [Validators.required, Validators.maxLength(299)]
      ),
      firstName: new FormControl(
        this.editing ? this.editingClient.firstName : null,
        [Validators.maxLength(299)]
      ),
      middleName: new FormControl(
        this.editing ? this.editingClient.middleName : null,
        [Validators.maxLength(299)]
      ),
      firstLastName: new FormControl(
        this.editing ? this.editingClient.firstLastName : null,
        [Validators.maxLength(299)]
      ),
      secondLastName: new FormControl(
        this.editing ? this.editingClient.secondLastName : null,
        [Validators.required]
      ),
      email: new FormControl(
        this.editing ? this.editingClient.email : null,
        [Validators.required, Validators.email, Validators.maxLength(49)]
      ),
      phone: new FormControl(
        this.editing ? this.editingClient.phone : null,
        [Validators.required, Validators.pattern('^[0-9]*$'), Validators.maxLength(20)]
      ),
      address: new FormControl(
        this.editing ? this.editingClient.address : null,
        [Validators.required, Validators.maxLength(199)]
      ),
      city: new FormControl(
        this.editing ? this.editingClient.city : null,
        [Validators.required]
      ),
      department: new FormControl(
        this.editing ? this.editingClient.department : null,
        [Validators.required]
      ),
      municipality: new FormControl(
        this.editing ? this.editingClient.municipality : null,
      ),
      country: new FormControl(
        this.editing ? this.editingClient.country : null,
        [Validators.required]
      ),
      postalCode: new FormControl(
        this.editing ? this.editingClient.postalCode : null,
        [Validators.required, Validators.maxLength(9)]
      ),
      observation: new FormControl(
        this.editing ? this.editingClient.observation : null,
        [ Validators.maxLength(99)]
      ),
    });
  }

  ngOnDestroy() {
    this.clientStatusSubscription.unsubscribe();
  }

  /**
   * Método que cancela la creación o edición de un dispositivo.
   */
  public cancel() {
    this.modal.destroy();
  }

  public saveClient() {
    // tslint:disable-next-line:forin
    for (const i in this.addEditClientForm.controls) {
      this.addEditClientForm.controls[i].markAsDirty();
      this.addEditClientForm.controls[i].updateValueAndValidity();
    }
    if (this.addEditClientForm.invalid) {
      return;
    }
    this.store.dispatch(ServicesStatusActions.ShowLoadingAction({ signature: ServicesSignatures.CLIENTS_ADD }));
    if (this.editing) {
      this.store.dispatch(ClientsActions.EditClientsAction({clients: {...this.addEditClientForm.value, _id: this.editingClient._id }}));
    } else {
      // tslint:disable-next-line:max-line-length
      this.store.dispatch(ClientsActions.CreateClientsAction({clients: this.addEditClientForm.value}));
    }
  }

  public typeDocumentValidator( control: FormControl): {[s: string]: boolean} {
    this.nameFlag = control.value === '31' || control.value === '50' || control.value === '91';
    if (this.addEditClientForm && this.nameFlag) {
      this.addEditClientForm.controls.firstName.setValue('');
      this.addEditClientForm.controls.middleName.setValue('');
      this.addEditClientForm.controls.firstLastName.setValue('');
      this.addEditClientForm.controls.secondLastName.setValue('');
      this.addEditClientForm.controls.firstName.setValidators([
        Validators.maxLength(299)
      ]);
      this.addEditClientForm.controls.middleName.setValidators([
        Validators.maxLength(299)
      ]);
      this.addEditClientForm.controls.firstLastName.setValidators([
        Validators.maxLength(299)
      ]);
      this.addEditClientForm.controls.secondLastName.setValidators([
        Validators.maxLength(299)
      ]);
    } else if (this.addEditClientForm) {
      this.addEditClientForm.controls.firstName.setValidators([
        Validators.maxLength(299),
        Validators.required
      ]);
      this.addEditClientForm.controls.middleName.setValidators([
        Validators.maxLength(299),
        Validators.required
      ]);
      this.addEditClientForm.controls.firstLastName.setValidators([
        Validators.maxLength(299),
        Validators.required
      ]);
      this.addEditClientForm.controls.secondLastName.setValidators([
        Validators.maxLength(299),
        Validators.required
      ]);
    }
    return null;
  }
}
