import {Component, OnDestroy, OnInit} from '@angular/core';
import {select, Store} from '@ngrx/store';
import * as fromClients from '@state/clients';
import {Observable, Subscription} from 'rxjs';
import {ServicesStatus} from '@state/services-status/services-status.interface';
import {Clients} from '@app/features/client/models/Clients';
import * as ServicesStatusSelectors from '@state/services-status/services-status.selector';
import {ServicesSignatures} from '@shared/enums/services-signatures.enum';
import * as ClientsActions from '@state/clients/clients.actions';
import {NzModalService} from 'ng-zorro-antd';
import {AddEditClientFormComponent} from '@app/features/client/containers/add-edit-client-form/add-edit-client-form.component';
import * as ServicesStatusActions from '@state/services-status/services-status.actions';

@Component({
  selector: 'fact-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.scss']
})
export class ClientComponent implements OnInit, OnDestroy {


  public clients: Observable<Clients[]>;

  public querySearch: string;

  public addOrEditClientStatus: Observable<ServicesStatus>;


  constructor(
    private store: Store<fromClients.State>,
    private modalService: NzModalService
  ) {
    this.clients = this.store.pipe(select(fromClients.getClients));
    this.addOrEditClientStatus = this.store.pipe(
      select(ServicesStatusSelectors.getServiceStateById, { signature: ServicesSignatures.CLIENTS })
    );
  }

  ngOnInit() {
    this.store.dispatch(ServicesStatusActions.ShowLoadingAction({ signature: ServicesSignatures.CLIENTS }));
    this.store.dispatch(ClientsActions.GetClientsAction({ signature: ServicesSignatures.CLIENTS }));
  }
  addOrEditClient( client: Clients = null) {
    this.modalService.create({
      nzContent: AddEditClientFormComponent,
      nzClassName: 'app-add-edit-form',
      nzWidth: 1000,
      nzComponentParams: {
        editingClient: client
      }
    });
  }
  // tslint:disable-next-line:variable-name
  deleteClient(_id: string) {
    this.store.dispatch(ClientsActions.DeleteClientsAction({ _id, signature: ServicesSignatures.CLIENTS }));
  }

  ngOnDestroy(): void {
    this.modalService.closeAll();
  }

}
