import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import { Clients } from '../../models/Clients';
import {FactSearchService} from '@shared/enums/fact-search-service.enum';
import {ServicesStatus} from '@state/services-status/services-status.interface';
import {Company} from '@app/features/companies/models/Company';

@Component({
  selector: 'fact-table-client',
  templateUrl: './table-client.component.html',
  styleUrls: ['./table-client.component.scss']
})
export class TableClientComponent implements OnInit {

  get servicesTypes() {
    return FactSearchService;
  }

  /**
   * Propiedad que recibe el estado en el que está el consumo del servicio de obtener dispositivos.
   */
  @Input()
  serviceStatus: ServicesStatus;

  @Input()
  clients: Clients[];

  /**
   * Evento que emite el dispositivo que va a ser editado.
   */
  @Output()
  editClientEmitter: EventEmitter<Clients> = new EventEmitter<Clients>();

  /**
   * Evento que emite el dispositivo que va a ser eliminado.
   */
  @Output()
  deleteClientEmitter: EventEmitter<string> = new EventEmitter<string>();

  /**
   * Evento que emite el dispositivo que va a ser editado.
   */
  @Output()
  editCompanyEmitter: EventEmitter<Company> = new EventEmitter<Company>();


  @Input()
  querySearch: string;

  constructor() { }

  ngOnInit() {
  }

}
