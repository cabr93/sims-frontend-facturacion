import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {Paths} from '@shared/enums/paths.enum';
import {AuthService} from '@core/services/auth.service';
import {User} from '@core/models/User';
import {TranslateService} from '@ngx-translate/core';
import {ItemMenu} from '@shared/interfaces/item-menu.interface';
import {IdleService} from '@core/services/idle.service';

@Component({
  selector: 'fact-features',
  templateUrl: './features.component.html',
  styleUrls: ['./features.component.scss']
})
export class FeaturesComponent implements OnInit {

  public userInfo: User;
  public mainMenu: Array<ItemMenu>;

  public flag: boolean;
  constructor(
    private router: Router,
    private authService: AuthService,
    private translateService: TranslateService,
    private pgvIdleService: IdleService
  ) {
    this.flag = false;
  }

  ngOnInit() {
    this.userInfo = AuthService.currentUser;
    this.translateService.get('mainMenu').subscribe(
      (items: Array<ItemMenu>) => {
        const user = AuthService.currentUser;
        this.mainMenu = items.filter( item => item.role === user.rol);
      }
    );
    this.pgvIdleService.start(20);
  }

  logOut( ) {
    this.authService.logout();
  }

}
