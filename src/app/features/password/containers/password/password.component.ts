import { Component, OnInit } from '@angular/core';
import {ChangePasswordInterface} from '@app/features/password/interfaces/change-password.interface';
import * as ServicesStatusActions from '@state/services-status/services-status.actions';
import {ServicesSignatures} from '@shared/enums/services-signatures.enum';
import * as AuthActions from '@state/auth/auth.actions';
import {Store} from '@ngrx/store';
import * as fromAuth from '@state/auth';

@Component({
  selector: 'fact-change-password',
  templateUrl: './password.component.html',
  styleUrls: ['./password.component.scss']
})
export class PasswordComponent implements OnInit {

  constructor(
    private store: Store<fromAuth.State>,
    ) { }

  ngOnInit() {
  }

  changePsw(passwords: ChangePasswordInterface): void {
    this.store.dispatch(ServicesStatusActions.ShowLoadingAction({ signature: ServicesSignatures.CHANGE_PSW }));
    const action = AuthActions.ChangePswAction({ passwords, signature: ServicesSignatures.CHANGE_PSW });
    this.store.dispatch(action);
  }

}
