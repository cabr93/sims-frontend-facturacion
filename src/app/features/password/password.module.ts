import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PasswordRoutingModule } from './password-routing.module';
import { PasswordComponent } from './containers/password/password.component';
import { PasswordFormComponent } from './components/password-form/password-form.component';
import {SharedModule} from '@shared/shared.module';
import {FormsModule} from '@angular/forms';


@NgModule({
  declarations: [PasswordComponent, PasswordFormComponent],
  imports: [
    CommonModule,
    PasswordRoutingModule,
    SharedModule,
    FormsModule
  ]
})
export class PasswordModule { }
