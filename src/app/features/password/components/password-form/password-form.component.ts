import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ChangePasswordInterface} from '@app/features/password/interfaces/change-password.interface';

@Component({
  selector: 'fact-change-password-form',
  templateUrl: './password-form.component.html',
  styleUrls: ['./password-form.component.scss']
})
export class PasswordFormComponent implements OnInit {

  @Output()
  emitterChangePsw: EventEmitter<ChangePasswordInterface> = new EventEmitter<ChangePasswordInterface>();

  public changePasswordForm: FormGroup;
  constructor() { }

  ngOnInit() {
    this.changePasswordForm = new FormGroup({
      oldPassword: new FormControl('', [Validators.required]),
      newPassword: new FormControl('' ),
      newPassword2: new FormControl('', [Validators.required])
    },  this.errorPassword.bind(this));
  }
  changePassword() {
    // tslint:disable-next-line:forin
    for (const i in this.changePasswordForm.controls) {
      this.changePasswordForm.controls[i].markAsDirty();
      this.changePasswordForm.controls[i].updateValueAndValidity();
    }
    if (this.changePasswordForm.invalid) {
      return;
    }

    this.emitterChangePsw.emit({...this.changePasswordForm.value});

  }

  public errorPassword(): {[s: string]: boolean} {
    const form: any = this.changePasswordForm;
    if (!form) {
      return null;
    }
    const oldPassword = form.controls.oldPassword.value;
    const newPassword = form.controls.newPassword.value;
    const newPasswordTouch = form.controls.newPassword.pristine;
    const newPassword2 = form.controls.newPassword2.value;
    const newPassword2Touch = form.controls.newPassword2.pristine;
    if (!newPasswordTouch && newPassword === '') {
      form.controls.newPassword.setErrors({required: true});
    } else if (oldPassword === newPassword && !newPasswordTouch ) {
      form.controls.newPassword.setErrors({notSame: true});
    } else {
      form.controls.newPassword.setErrors( null);
    }
    if (!newPassword2Touch && newPassword2 === '') {
      form.controls.newPassword2.setErrors({required: true});
    } else if (newPassword2 !== newPassword && !newPassword2Touch) {
      form.controls.newPassword2.setErrors({notSame: true});
    } else {
      form.controls.newPassword2.setErrors( null);
    }
    return null;
  }


}
