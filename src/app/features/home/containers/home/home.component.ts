import { Component, OnInit } from '@angular/core';
import {AuthService} from '@core/services/auth.service';

@Component({
  selector: 'fact-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  user: any;
  flag: boolean;
  constructor() { }

  ngOnInit() {
    this.user = AuthService.currentUser;
    this.user.rol === 'ADMINISTRATOR' ? this.flag = false : this.flag = true;
  }

}
