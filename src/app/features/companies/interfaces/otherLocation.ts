export interface OtherLocation {
  location: string;
  postalCode: string;
  address: string,
}
