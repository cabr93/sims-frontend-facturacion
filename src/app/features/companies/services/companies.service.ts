import { Injectable } from '@angular/core';
import {HttpInvokeService} from '@core/services/http-invoke.service';
import {Observable} from 'rxjs';
import {ServicesSignatures} from '@shared/enums/services-signatures.enum';
import {Company} from '@app/features/companies/models/Company';
import {Clients} from '@app/features/client/models/Clients';

@Injectable({
  providedIn: 'root'
})
export class CompaniesService {

  constructor(
    private httpInvokeService: HttpInvokeService
  ) { }

  public getCompanies(): Observable<Company[]> {
    return this.httpInvokeService
      .get<Company[]>('/companies', ServicesSignatures.COMPANY , false);
  }

  public newCompany(company: Company): Observable<any> {
    return this.httpInvokeService
      .post<Company, any>('/companies', company, ServicesSignatures.COMPANY , true);
  }
  public editCompany(company: Company): Observable<any> {
    return this.httpInvokeService
      .put<Company, any>('/companies', company, ServicesSignatures.COMPANY , true);
  }
  public deleteCompany( id: string): Observable<any> {
    return this.httpInvokeService
      .delete<string, any>(`/companies?id=${id}`, ServicesSignatures.COMPANY , true);
  }
}
