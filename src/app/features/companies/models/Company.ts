import {OtherLocation} from '@app/features/companies/interfaces/otherLocation';

export class Company {
  constructor(
    // tslint:disable-next-line:variable-name
    public _id: string,
    public typeId: string,
    public typeDocId: string,
    public numberDocId: string,
    public verificationCode: string,
    public taxRegime: string,
    public fiscalResponsibility: string,
    public operationType: string,
    public commercialName: string,
    public socialReason: string,
    public area: string,
    public legalRepresentative: string,
    public docId: string,
    public consortium: string,
    public consortiumPercent: string,
    public email: string,
    public phone: string,
    public webSite: string,
    public observation: string,
    public mainLocation: string,
    public postalCode: string,
    public address: string,
    public otherLocations: Array<OtherLocation>
  ) {}
  getStringify(): string {
    return `${this.numberDocId}${this.commercialName}`;
  }
}
