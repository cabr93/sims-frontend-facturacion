import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Company} from '@app/features/companies/models/Company';
import {FactSearchService} from '@shared/enums/fact-search-service.enum';

@Component({
  selector: 'fact-table-business',
  templateUrl: './table-companies.component.html',
  styleUrls: ['./table-companies.component.scss']
})
export class TableCompaniesComponent implements OnInit {
  get servicesTypes() {
    return FactSearchService;
  }
  @Input()
  companies: Company[];

  @Input()
  querySearch: string;

  /**
   * Evento que emite el dispositivo que va a ser eliminado.
   */
  @Output()
  deleteCompanyEmitter: EventEmitter<string> = new EventEmitter<string>();

  /**
   * Evento que emite el dispositivo que va a ser editado.
   */
  @Output()
  editCompanyEmitter: EventEmitter<string> = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
  }

}
