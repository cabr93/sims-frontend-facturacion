import { NgModule } from '@angular/core';
import { CompaniesRoutingModule } from './companies-routing.module';
import { CompaniesComponent } from './containers/companies/companies.component';
import { TableCompaniesComponent } from './component/table-companies/table-companies.component';
import {SharedModule} from '@shared/shared.module';
import {FormsModule} from '@angular/forms';
import { AddEditCompanyFormComponent } from './containers/add-edit-company-form/add-edit-company-form.component';
import {StoreModule} from '@ngrx/store';
import * as companiesReducer from '@state/companies/companies.reducer';
import {EffectsModule} from '@ngrx/effects';
import {CompaniesEffects} from '@state/companies/companies.effects';


@NgModule({
  declarations: [
    CompaniesComponent,
    TableCompaniesComponent,
    AddEditCompanyFormComponent
  ],
  entryComponents: [
    AddEditCompanyFormComponent
  ],
  imports: [
    CompaniesRoutingModule,
    SharedModule,
    FormsModule,
    StoreModule.forFeature('companies', companiesReducer.reducer),
    EffectsModule.forFeature([ CompaniesEffects ])
  ]
})
export class CompaniesModule { }
