import {Component, OnDestroy, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {ServicesStatus} from '@state/services-status/services-status.interface';
import {NzModalService} from 'ng-zorro-antd';
import {AddEditCompanyFormComponent} from '@app/features/companies/containers/add-edit-company-form/add-edit-company-form.component';
import {select, Store} from '@ngrx/store';
import * as fromCompanies from '@state/companies';
import {Company} from '@app/features/companies/models/Company';
import * as ServicesStatusSelectors from '@state/services-status/services-status.selector';
import {ServicesSignatures} from '@shared/enums/services-signatures.enum';
import * as ServicesStatusActions from '@state/services-status/services-status.actions';
import * as CompaniesActions from '@state/companies/companies.actions';

@Component({
  selector: 'fact-business',
  templateUrl: './companies.component.html',
  styleUrls: ['./companies.component.scss']
})
export class CompaniesComponent implements OnInit, OnDestroy {


  public querySearch: string;

  public companies: Observable<Company[]>;

  public addOrEditCompaniesStatus: Observable<ServicesStatus>;

  constructor(
    private store: Store<fromCompanies.State>,
    private modalService: NzModalService
  ) {
    this.companies = this.store.pipe(select(fromCompanies.getCompanies));
    this.addOrEditCompaniesStatus = this.store.pipe(
      select(ServicesStatusSelectors.getServiceStateById, { signature: ServicesSignatures.COMPANY })
    );
  }

  ngOnInit() {
    this.store.dispatch(ServicesStatusActions.ShowLoadingAction({ signature: ServicesSignatures.COMPANY }));
    this.store.dispatch(CompaniesActions.GetCompaniesAction({ signature: ServicesSignatures.COMPANY }));
  }

  // tslint:disable-next-line:no-shadowed-variable
  addOrEditCompany(Company: any = null) {
    this.modalService.create({
      nzContent: AddEditCompanyFormComponent,
      nzClassName: 'app-add-edit-form',
      nzWidth: 1000,
      nzComponentParams: {
        editingCompany: Company
      }
    });
  }

  // tslint:disable-next-line:variable-name
  deleteCompany(_id: string) {
    this.store.dispatch(CompaniesActions.DeleteCompaniesAction({ _id, signature: ServicesSignatures.COMPANY }));
  }

  ngOnDestroy(): void {
    this.modalService.closeAll();
  }

}
