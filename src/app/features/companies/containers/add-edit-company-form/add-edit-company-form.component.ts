import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Clients} from '@app/features/client/models/Clients';
import {FormArray, FormControl, FormGroup, Validators} from '@angular/forms';
import {ItemSelect} from '@shared/interfaces/item-select.interface';
import {TranslateService} from '@ngx-translate/core';
import * as ServicesStatusActions from '@state/services-status/services-status.actions';
import {ServicesSignatures} from '@shared/enums/services-signatures.enum';
import * as CompaniesActions from '@state/companies/companies.actions';
import {select, Store} from '@ngrx/store';
import * as fromCompanies from '@state/companies';
import * as ServicesStatusSelectors from '@state/services-status/services-status.selector';
import {MessageType} from '@core/enums/message-type';
import {Subscription} from 'rxjs';
import {ServicesStatus} from '@state/services-status/services-status.interface';
import {ToastService} from '@core/services/toast.service';
import {NzModalRef} from 'ng-zorro-antd';
import {Company} from '@app/features/companies/models/Company';


@Component({
  selector: 'fact-add-edit-company-form',
  templateUrl: './add-edit-company-form.component.html',
  styleUrls: ['./add-edit-company-form.component.scss']
})
export class AddEditCompanyFormComponent implements OnInit, OnDestroy {

  public addEditCompanyForm: FormGroup;
  public editing: boolean;
  public companyTypeof: Array<ItemSelect>;
  public companyTypeDocId: Array<ItemSelect>;
  public fiscalResponsibility: Array<ItemSelect>;
  public operationType: Array<ItemSelect>;
  public cities: Array<ItemSelect>;
  private loaded: boolean;
  public companyStatusSubscription: Subscription;
  public addOrEditCompanyStatus: ServicesStatus;



  @Input()
  editingCompany: Company;
  constructor(
    private modal: NzModalRef,
    private translateService: TranslateService,
    private store: Store<fromCompanies.State>,
    private toastService: ToastService
  ) {
    this.loaded = false;
    this.companyStatusSubscription = this.store.pipe(
      select(ServicesStatusSelectors.getServiceStateById, { signature: ServicesSignatures.COMPANY_ADD })
    ).subscribe(status => {
      this.addOrEditCompanyStatus = status || { loading: false, loaded: false, error: '', signature: ServicesSignatures.COMPANY_ADD };
      console.log(this.addOrEditCompanyStatus.loaded);
      if (this.addOrEditCompanyStatus.loaded && !this.loaded) {
        this.loaded = true;
        const key = this.editing ? 'successCompanyUpdated' : 'successCompanySaved';
        this.toastService.showMessage(MessageType.SUCCESS, key);
        this.store.dispatch(ServicesStatusActions.RemoveStatusAction({ signature: ServicesSignatures.COMPANY_ADD }));
        this.modal.destroy();
      }
    });
  }

  ngOnInit() {
    this.editing = !!this.editingCompany;
    this.translateService.get( 'ClientTypeId').subscribe(items => this.companyTypeof = items);
    this.translateService.get( 'ClientTypeDocId').subscribe(items => this.companyTypeDocId = items);
    this.translateService.get( 'ClientFiscalResponsibility').subscribe(items => this.fiscalResponsibility = items);
    this.translateService.get( 'ClientOperationType').subscribe(items => this.operationType = items);
    this.translateService.get( 'ClientCity').subscribe(items => this.cities = items);

    this.addEditCompanyForm = new FormGroup({
      typeId: new FormControl(
        this.editing ? this.editingCompany.typeId : null,
        [Validators.required]
      ),
      typeDocId: new FormControl(
        this.editing ? this.editingCompany.typeDocId : null,
        [Validators.required]
      ),
      numberDocId: new FormControl(
        this.editing ? this.editingCompany.numberDocId : null,
        [ Validators.required, Validators.pattern('^[0-9]*$'), Validators.maxLength(49)]
      ),
      verificationCode: new FormControl(
        this.editing ? this.editingCompany.verificationCode : null,
        [ Validators.maxLength(1), Validators.pattern('^[0-9]*$')]
      ),
      taxRegime: new FormControl(
        this.editing ? this.editingCompany.taxRegime : null,
        [Validators.required]
      ),
      fiscalResponsibility: new FormControl(
        this.editing ? this.editingCompany.fiscalResponsibility : null,
        [Validators.required]
      ),
      operationType: new FormControl(
        this.editing ? this.editingCompany.operationType : null,
        [Validators.required]
      ),
      commercialName: new FormControl(
        this.editing ? this.editingCompany.commercialName : null,
        [Validators.required, Validators.maxLength(299)]
      ),
      area: new FormControl(
        this.editing ? this.editingCompany.area : null,
        [Validators.required ]
      ),
      socialReason: new FormControl(
        this.editing ? this.editingCompany.socialReason : null,
        [Validators.required, Validators.maxLength(299)]
      ),
      legalRepresentative: new FormControl(
        this.editing ? this.editingCompany.legalRepresentative : null,
        [Validators.required]
      ),
      docId: new FormControl(
        this.editing ? this.editingCompany.docId : null,
        [Validators.required]
      ),
      consortium: new FormControl(
        this.editing ? this.editingCompany.consortium : null,
        [Validators.required]
      ),
      consortiumPercent: new FormControl(
        this.editing ? this.editingCompany.consortiumPercent : null,
        [Validators.maxLength(5)]
      ),
      email: new FormControl(
        this.editing ? this.editingCompany.email : null,
        [Validators.required, Validators.email, Validators.maxLength(49)]
      ),
      phone: new FormControl(
        this.editing ? this.editingCompany.phone : null,
        [Validators.required, Validators.pattern('^[0-9]*$'), Validators.maxLength(20)]
      ),
      webSite: new FormControl(
        this.editing ? this.editingCompany.webSite : null,
        [Validators.required]
      ),
      observation: new FormControl(
        this.editing ? this.editingCompany.observation : null,
        [Validators.maxLength(99)]
      ),
      mainLocation: new FormControl(
        this.editing ? this.editingCompany.mainLocation : null,
        [Validators.required ]
      ),
      postalCode: new FormControl(
        this.editing ? this.editingCompany.postalCode : null,
        [Validators.required ]
      ),
      address: new FormControl(
        this.editing ? this.editingCompany.address : null,
        [Validators.required ]
      ),
      otherLocations: new FormArray([])
    });
    if (this.editing) {
      this.updateLocations();
    }
  }

  ngOnDestroy() {
    this.companyStatusSubscription.unsubscribe();
  }

  public updateLocations() {
    // tslint:disable-next-line:forin
    for (const i in this.editingCompany.otherLocations) {
      ( this.addEditCompanyForm.controls.otherLocations as FormArray).push(
        new FormGroup({
          location: new FormControl(this.editingCompany.otherLocations[i].location, [Validators.required] ),
          postalCode: new FormControl(this.editingCompany.otherLocations[i].postalCode, [Validators.required] ),
          address: new FormControl(this.editingCompany.otherLocations[i].address, [Validators.required] ),
        }, this.cityValidator.bind(this))
      );
    }
  }
  public saveCompany() {
    // tslint:disable-next-line:forin
    for (const i in this.addEditCompanyForm.controls) {
      this.addEditCompanyForm.controls[i].markAsDirty();
      this.addEditCompanyForm.controls[i].updateValueAndValidity();
    }
    // @ts-ignore
    // tslint:disable-next-line:forin
    for (const i in this.addEditCompanyForm.controls.otherLocations.controls) {
      // @ts-ignore
      // tslint:disable-next-line:forin
      for (const j in this.addEditCompanyForm.controls.otherLocations.controls[i].controls) {
        // @ts-ignore
        this.addEditCompanyForm.controls.otherLocations.controls[i].controls[j].markAsDirty();
        // @ts-ignore
        this.addEditCompanyForm.controls.otherLocations.controls[i].controls[j].updateValueAndValidity();
      }
    }
    if (this.addEditCompanyForm.invalid) {
      return;
    }

    this.store.dispatch(ServicesStatusActions.ShowLoadingAction({ signature: ServicesSignatures.COMPANY_ADD }));
    if (this.editing) {
      this.store.dispatch(CompaniesActions.EditCompaniesAction(
        {
          company: {
          ...this.addEditCompanyForm.value,
            _id: this.editingCompany._id
          }
        }
      ));
    } else {
      // tslint:disable-next-line:max-line-length
      this.store.dispatch(CompaniesActions.CreateCompaniesAction({company: this.addEditCompanyForm.value}));
    }
  }

  public addOtherLocation() {
    ( this.addEditCompanyForm.controls.otherLocations as FormArray).push(
      new FormGroup({
        location: new FormControl('', [Validators.required] ),
        postalCode: new FormControl('', [Validators.required] ),
      }, this.cityValidator.bind(this))
    );
  }

  public deleteLocation(i: number) {
    ( this.addEditCompanyForm.controls.otherLocations as FormArray).removeAt(i);
  }

  public cityValidator( control: FormControl): {[s: string]: boolean} {
    const val = [];
    // @ts-ignore
    // tslint:disable-next-line:forin
    for (const i in this.addEditCompanyForm.controls.otherLocations.controls) {
      // @ts-ignore
      val.push(this.addEditCompanyForm.controls.otherLocations.controls[i].controls.location.value);
    }
    const change = new Array(val.length).fill(0); // create an empty array with length 45
    for (let i = 0; i <= val.length; i++) {
      for (let j = 0; j < val.length; j++) {
        if (i !== j && val[i] === val[j] && val[i] !== '') {
          change[i] = 1;
        }
      }
    }
    for (let i = 0; i < val.length; i++) {
      if (change[i] === 1) {
        // @ts-ignore
        this.addEditCompanyForm.controls.otherLocations.controls[i].controls.location.setErrors({notSame: true});
      } else if (val[i] !== '') {
        // @ts-ignore
        this.addEditCompanyForm.controls.otherLocations.controls[i].controls.location.setErrors( null);
      }
    }
    return null;
  }


}
