import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {FeaturesComponent} from '@app/features/features.component';
import {RolesGuard} from '@core/guards/roles.guard';
import {PswGuard} from '@core/guards/psw.guard';


const routes: Routes = [
  {
    path: '',
    component: FeaturesComponent,
    children: [
      {
        path: '',
        redirectTo: 'home'
      },
      {
        path: 'home',
        loadChildren: () => import('./home/home.module').then(m => m.HomeModule),
        canActivate: [PswGuard]
      },
      {
        path: 'bill',
        loadChildren: () => import('./bill-spool/bill-spool.module').then(m => m.BillSpoolModule),
        canActivate: [RolesGuard]
      },
      {
        path: 'historical',
        loadChildren: () => import('./historical/historical.module').then(m => m.HistoricalModule),
        canActivate: [RolesGuard]
      },
      {
        path: 'client',
        loadChildren: () => import('./client/client.module').then(m => m.ClientModule),
        canActivate: [RolesGuard]
      },
      {
        path: 'companies',
        loadChildren: () => import('./companies/companies.module').then(m => m.CompaniesModule),
        canActivate: [RolesGuard]
      },
      {
        path: 'change',
        loadChildren: () => import('./password/password.module').then(m => m.PasswordModule),
      },
      {
        path: 'users',
        loadChildren: () => import('./user/user.module').then(m => m.UserModule),
        canActivate: [RolesGuard]
      },
      {
        path: 'logs',
        loadChildren: () => import('./log/log.module').then(m => m.LogModule),
        canActivate: [RolesGuard]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FeaturesRoutingModule { }
