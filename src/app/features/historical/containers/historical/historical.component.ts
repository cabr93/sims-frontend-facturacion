import { Component, OnInit } from '@angular/core';
import * as ServicesStatusActions from '@state/services-status/services-status.actions';
import {ServicesSignatures} from '@shared/enums/services-signatures.enum';
import * as HistoricalActions from '@state/historical/historical.actions';
import {select, Store} from '@ngrx/store';
import * as fromHistorical from '@state/historical';
import {Observable} from 'rxjs';
import {Historical} from '@app/features/historical/models/Historical';

@Component({
  selector: 'fact-historical',
  templateUrl: './historical.component.html',
  styleUrls: ['./historical.component.scss']
})
export class HistoricalComponent implements OnInit {

  public querySearch: string;

  public historical: Observable<Historical[]>;

  constructor(
    private store: Store<fromHistorical.State>,
  ) {
    this.historical = this.store.pipe(select(fromHistorical.getHistorical));
  }

  ngOnInit() {
    this.store.dispatch(ServicesStatusActions.ShowLoadingAction({ signature: ServicesSignatures.HISTORICAL }));
    this.store.dispatch(HistoricalActions.GetHistoricalAction({ signature: ServicesSignatures.HISTORICAL }));
  }

}
