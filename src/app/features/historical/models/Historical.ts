import {CompanyInterface} from '@app/features/historical/interfaces/company.interface';
import {ClientInterface} from '@app/features/historical/interfaces/client.interface';

export class Historical {
  constructor(
    public state: boolean,
    public company: CompanyInterface,
    public client: ClientInterface,
    public spool: string,
    public total: string,
    public prefix: string,
    public consecutive: string,
    public currency: string,
    public urlDian: string,
    public created: string
  ) {
  }

  getStringify(): string {
    return `${this.consecutive}${this.prefix}${this.company.name}${this.client.name}`;
  }
}
