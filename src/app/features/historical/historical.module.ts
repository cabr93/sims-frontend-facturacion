import { NgModule } from '@angular/core';

import { HistoricalRoutingModule } from './historical-routing.module';
import { HistoricalComponent } from './containers/historical/historical.component';
import { TableHistoricalComponent } from './components/table-historical/table-historical.component';
import {SharedModule} from '@shared/shared.module';
import {FormsModule} from '@angular/forms';
import {StoreModule} from '@ngrx/store';
import * as historicalReducer from '@state/historical/historical.reducer';
import {EffectsModule} from '@ngrx/effects';
import {HistoricalEffects} from '@state/historical/historical.effects';


@NgModule({
  declarations: [
    HistoricalComponent,
    TableHistoricalComponent
  ],
  imports: [
    SharedModule,
    HistoricalRoutingModule,
    FormsModule,
    StoreModule.forFeature('historical', historicalReducer.reducer),
    EffectsModule.forFeature([ HistoricalEffects ])
  ]
})
export class HistoricalModule { }
