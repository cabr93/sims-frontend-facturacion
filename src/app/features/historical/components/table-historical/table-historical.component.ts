import {Component, Input, OnInit} from '@angular/core';
import {Historical} from '@app/features/historical/models/Historical';
import {FactSearchService} from '@shared/enums/fact-search-service.enum';

@Component({
  selector: 'fact-table-historical',
  templateUrl: './table-historical.component.html',
  styleUrls: ['./table-historical.component.scss']
})
export class TableHistoricalComponent implements OnInit {
  get servicesTypes() {
    return FactSearchService;
  }
  @Input()
  historical: Historical[];

  @Input()
  querySearch: string;

  constructor() { }

  ngOnInit() {
  }

}
