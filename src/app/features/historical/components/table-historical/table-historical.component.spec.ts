import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableHistoricalComponent } from './table-historical.component';

describe('TableHistoricalComponent', () => {
  let component: TableHistoricalComponent;
  let fixture: ComponentFixture<TableHistoricalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableHistoricalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableHistoricalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
