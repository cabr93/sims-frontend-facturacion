export interface CompanyInterface {
  name: string;
  id: string;
  doc: string;
}
