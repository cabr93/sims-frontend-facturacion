export interface ClientInterface {
  name: string;
  doc: string;
  id: string;
}
