import { Injectable } from '@angular/core';
import {HttpInvokeService} from '@core/services/http-invoke.service';
import {Observable} from 'rxjs';
import {ServicesSignatures} from '@shared/enums/services-signatures.enum';
import {Historical} from '@app/features/historical/models/Historical';

@Injectable({
  providedIn: 'root'
})
export class HistoricalService {

  constructor(
    private httpInvokeService: HttpInvokeService
  ) { }

  public getHistorical(): Observable<Historical[]> {
    return this.httpInvokeService
      .get<Historical[]>('/historical', ServicesSignatures.HISTORICAL , false);
  }
}
