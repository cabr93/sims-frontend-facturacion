import { NgModule } from '@angular/core';
import {TranslateModule} from '@ngx-translate/core';
import {CommonModule} from '@angular/common';
import {NgZorroAntdModule} from 'ng-zorro-antd';
import {RouterModule} from '@angular/router';
import {ReactiveFormsModule} from '@angular/forms';
import { SearchPipe } from './pipes/search.pipe';
import { TranslatearrayPipe } from './pipes/translatearray.pipe';

const modules = [
  CommonModule,
  NgZorroAntdModule,
  RouterModule,
  ReactiveFormsModule
];

const pipes = [
  SearchPipe,
  TranslatearrayPipe
];

@NgModule({
  declarations: [
    ...pipes
  ],
  imports: [
    TranslateModule.forChild(),
      ...modules
  ],
  exports: [
    CommonModule,
    TranslateModule,
    ...modules,
    ...pipes
  ]
})
export class SharedModule { }
