export enum FactSearchService {
  CLIENTS = 'clients',
  COMPANY = 'companies',
  LOGS = 'logs',
  HISTORICAL = 'historical'
}
