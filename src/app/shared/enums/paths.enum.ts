export enum Paths {
  AUTH = 'auth',
  SIGN_IN = 'sign-in',
  MAIN = 'main',
  HOME = 'home',
  CHANGE = 'change'
}
