export interface ItemMenu {
  path: string;
  label: string;
  icon: string;
  role: string;
}
