import { Pipe, PipeTransform } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {map} from 'rxjs/operators';

@Pipe({
  name: 'translatearray'
})
export class TranslatearrayPipe implements PipeTransform {

  constructor( private translateService: TranslateService) {
  }
  transform(value: any, type: string): any {
    let translateArray = [];
    this.translateService.get( type).subscribe(items => translateArray = items);
    return translateArray.filter(item => item.key === value)[0].name;
  }

}
