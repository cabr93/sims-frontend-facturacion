import { Pipe, PipeTransform } from '@angular/core';
import {FactSearchServiceType} from '@shared/types/fact-search-service-type';
import {FactSearchService} from '@shared/enums/fact-search-service.enum';
import { plainToClass } from 'class-transformer';
import {Clients} from '@app/features/client/models/Clients';
import {Company} from '@app/features/companies/models/Company';
import {Logs} from '@app/features/log/models/Logs';
import {Historical} from '@app/features/historical/models/Historical';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

  transform(items: FactSearchServiceType[], value: string, type: FactSearchService): any {
    if (!items) {
      return [];
    }

    if (!value) {
      return  items;
    }

    const tmpItems = this.objectToClass(items, type);

    return tmpItems.filter(item => item.getStringify().toLowerCase().indexOf(value.toLocaleLowerCase()) > -1 );
  }

  private objectToClass(items: FactSearchServiceType[], type: FactSearchService): FactSearchServiceType[] {
    switch (type) {
      case FactSearchService.CLIENTS:
        return items.map(item => plainToClass(Clients, item));
      case FactSearchService.COMPANY:
        return items.map(item => plainToClass(Company, item));
      case FactSearchService.LOGS:
        return items.map(item => plainToClass(Logs, item));
      case FactSearchService.HISTORICAL:
        return items.map(item => plainToClass(Historical, item));
    }

  }

}
