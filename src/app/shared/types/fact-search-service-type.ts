import {Clients} from '@app/features/client/models/Clients';
import {Company} from '@app/features/companies/models/Company';
import {Logs} from '@app/features/log/models/Logs';
import {Historical} from '@app/features/historical/models/Historical';

export type FactSearchServiceType = Clients | Company | Logs | Historical;
