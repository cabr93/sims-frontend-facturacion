import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {AuthService} from '@core/services/auth.service';
import {Store} from '@ngrx/store';
import * as fromAuth from '@state/auth/index';
import {User} from '@core/models/User';
import * as AuthActions from '@state/auth/auth.actions';

@Component({
  selector: 'fact-root',
  template: '<router-outlet></router-outlet>'
})
export class AppComponent implements OnInit   {
  constructor(
    private translateService: TranslateService,
    private store: Store<fromAuth.State>
  ) {
  }
  ngOnInit() {
    this.translateService.setDefaultLang('es');
    if (AuthService.isLogin()) {
      const user: User = JSON.parse(localStorage.getItem('user')) ;
      AuthService.currentUser = user;
      this.store.dispatch(AuthActions.SignSuccessAction({user}));
    }
  }

}
