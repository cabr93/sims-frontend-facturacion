import {AuthState} from '@state/auth';
import { ActionReducerMap} from '@ngrx/store';
import * as fromAuth from './auth/auth.reducer';
import * as fromServicesStatus from './services-status/services-status.reducer';
import {ServicesState} from '@state/services-status/services-status.reducer';
/**
 *  Estructura inicial de la aplicacion
 */

export interface  State {
  auth: AuthState;
  servicesStatus: ServicesState;
}

/**
 * declaracion de los reducer que necesita la aplicacion desde el inicio
 */

export const appReducers: ActionReducerMap<State> = {
  auth: fromAuth.reducer,
  servicesStatus: fromServicesStatus.reducer
};

