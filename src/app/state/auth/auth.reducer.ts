/**
 * inicializacion del estado de autenticacion.
 */
import {AuthState} from '@state/auth/index';
import {Action, createReducer, on} from '@ngrx/store';
import * as AuthActions from './auth.actions';

const initialState: AuthState = {
  user: {
    userName: '',
    rol: ''
  }
};


/**
 * Crear reducer de autenticacion.
 */
const userReducer = createReducer(
  initialState,
  on(AuthActions.SignSuccessAction, (state, { user }) => ({
    ...state,
    user
  })),
);

/**
 * Inicialización del reducer de autenticación
 */
export function reducer(state: AuthState, action: Action) {
  return userReducer(state, action);
}

// tslint:disable-next-line:no-shadowed-variable
export function clearState(reducer) {
  return (state, action) => {

    if (action.type === AuthActions.ClearStoreAction.type) {
      state = {};
    }

    return reducer(state, action);
  };
}
