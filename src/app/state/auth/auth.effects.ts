import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import * as AuthActions from './auth.actions';
import * as ServicesStatusActions from '@state/services-status/services-status.actions';
import {catchError, exhaustMap, map} from 'rxjs/operators';
import {AuthService} from '@core/services/auth.service';
import {ServicesSignatures} from '@shared/enums/services-signatures.enum';
import {of} from 'rxjs';
import {Router} from '@angular/router';
import {Paths} from '@shared/enums/paths.enum';
import {Store} from '@ngrx/store';
import * as fromAuth from '@state/auth';
import {ToastService} from '@core/services/toast.service';
import {MessageType} from '@core/enums/message-type';
import {TranslateService} from '@ngx-translate/core';

@Injectable()
export class AuthEffects {
  constructor(
    private actions$: Actions,
    private authService: AuthService,
    private router: Router,
    private store: Store<fromAuth.State>,
    private toastService: ToastService,
    private translateService: TranslateService,
  ) {}
  signIn$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.SignInAction),
      exhaustMap( action =>
        this.authService.signIn(action.userReq).pipe(
          map( user => {
            this.router.navigate([Paths.MAIN]);
            localStorage.setItem('user', JSON.stringify(user));
            return AuthActions.SignSuccessAction({user});
          }),
          catchError( (err) => {
            return of( ServicesStatusActions.HideLoadingErrorsAction({signature: ServicesSignatures.SIGN_IN, error: 'x'}));
          })
        )
      )
    )
  );
  changePsw$ = createEffect( () =>
    this.actions$.pipe(
      ofType(AuthActions.ChangePswAction),
      exhaustMap( action =>
        this.authService.changePassword(action.passwords).pipe(
          map( message => {
            this.router.navigate([Paths.MAIN, Paths.HOME]);
            const messageEmail = this.translateService.instant('sendEmailChangePsw');
            this.store.dispatch(ServicesStatusActions.HideLoadingSuccessAction( {signature: ServicesSignatures.CHANGE_PSW}));
            this.toastService.showMessage(MessageType.SUCCESS, `${messageEmail}`);
            return AuthActions.ChangePasswordSuccessAction();
          }),
          catchError( e => {
            return of( ServicesStatusActions.HideLoadingErrorsAction({signature: ServicesSignatures.CHANGE_PSW, error: ''}));
          })
        )
      )
    )
  );
  forgotPsw$ = createEffect( () =>
    this.actions$.pipe(
      ofType(AuthActions.ForgotPasswordAction),
      exhaustMap( action =>
        this.authService.forgotPassword(action.data).pipe(
          map( () => {
            this.store.dispatch(ServicesStatusActions.HideLoadingSuccessAction( {signature: ServicesSignatures.FORGOT_PSW}));
            return AuthActions.ForgotPasswordSuccessAction();
          }),
          catchError( e => {
            return of( ServicesStatusActions.HideLoadingErrorsAction({signature: ServicesSignatures.FORGOT_PSW, error: ''}));
          })
        )
      )
    )
  );
}
