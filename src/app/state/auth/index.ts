import {User} from '@core/models/User';
import * as fromApp from '@state/app.state';

/**
 * Estructura del estado de la sección de autenticación
 */

export interface AuthState {
  user: User;
}

/**
 * Extención del estado principal
 */
export interface State extends fromApp.State {
  user: AuthState;
}



