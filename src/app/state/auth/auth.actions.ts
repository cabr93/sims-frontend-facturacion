import {createAction, props} from '@ngrx/store';
import {SignInRequest} from '@core/authentication/dto/SignInRequest';
import {User} from '@core/models/User';
import {ChangePasswordInterface} from '@app/features/password/interfaces/change-password.interface';
import {UserForgotPsw} from '@core/authentication/interfaces/user-forgot-psw.interface';


/**
 * Iniciar sesión
 */
export const SignInAction = createAction(
  '[USER] Signing in',
  props<{ userReq: SignInRequest, signature: string}>()
);

/**
 * Inicio de sesión éxitoso
 */
export const SignSuccessAction = createAction(
  '[USER] signing user success',
  props<{ user: User }>()
);


export const ChangePswAction = createAction(
  '[USER] Change password',
  props<{ passwords: ChangePasswordInterface, signature: string}>()
);

export const ChangePasswordSuccessAction = createAction(
  '[User] change password send success'
);

export const ClearStoreAction = createAction(
  '[User] Reset store'
);

export const ForgotPasswordAction = createAction(
  '[User] forgot password',
  props<{ data: UserForgotPsw, signature: string}>()
);


export const ForgotPasswordSuccessAction = createAction(
  '[User] forgot password success'
);
