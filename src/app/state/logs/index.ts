import {Logs} from '@app/features/log/models/Logs';
import * as fromApp from './../app.state';
import {createFeatureSelector, createSelector} from '@ngrx/store';


export interface LogsState {
  logs: Logs[];
}

export interface State extends fromApp.State {
  logs: LogsState;
}

export const getLogsState = createFeatureSelector<LogsState>('logs');

export const getLogs = createSelector(
  getLogsState,
  state => state.logs
);
