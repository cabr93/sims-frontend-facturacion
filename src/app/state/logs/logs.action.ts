import {createAction, props} from '@ngrx/store';
import {Logs} from '@app/features/log/models/Logs';

export const GetLogsAction = createAction(
  '[LOGS] Get logs',
  props<{signature: string}>()
);

export const SetLogsAction = createAction(
  '[LOGS] Set logs',
  props<{logs: Logs[]}>()
);

