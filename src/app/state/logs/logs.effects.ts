import {Injectable} from '@angular/core';
import {Store} from '@ngrx/store';
import * as fromApp from './../app.state';
import {LogService} from '@app/features/log/services/log.service';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import * as LogsActions from './logs.action';
import {catchError, exhaustMap, map} from 'rxjs/operators';
import * as ServicesStatusActions from '@state/services-status/services-status.actions';
import {ServicesSignatures} from '@shared/enums/services-signatures.enum';
import {HttpErrorResponse} from '@angular/common/http';
import { of } from 'rxjs';

@Injectable()
export class LogsEffects {
  constructor(
    private action$: Actions,
    private store: Store<fromApp.State>,
    private logService: LogService
  ) {}

  $getLogs = createEffect( () =>
    this.action$.pipe(
      ofType(LogsActions.GetLogsAction),
      exhaustMap( action =>
        this.logService.getLogs().pipe(
          map( logs => {
            this.store.dispatch(ServicesStatusActions.HideLoadingSuccessAction( {signature: ServicesSignatures.LOGS}));
            return LogsActions.SetLogsAction( {logs});
          }),
          catchError( (e: HttpErrorResponse) => {
            return of(ServicesStatusActions.HideLoadingErrorsAction( {signature: ServicesSignatures.LOGS, error: String(e.status)}));
          })
        )
      )
    )
  );
}
