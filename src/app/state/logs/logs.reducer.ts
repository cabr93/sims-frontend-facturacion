import {LogsState} from '@state/logs/index';
import {Action, createReducer, on} from '@ngrx/store';
import * as LogsActions from './logs.action';


const initialState: LogsState = {
  logs: []
};

const logsReducer = createReducer(
  initialState,
  on(LogsActions.SetLogsAction, (state, {logs}) => ({
    ...state,
    logs
  }))
);

export function reducer( state: LogsState, action: Action) {
  return logsReducer(state, action);
}


