import {createAction, props} from '@ngrx/store';
import {ExcelData} from '@app/features/bill-spool/interfaces/excel-data.interface';
import {Clients} from '@app/features/client/models/Clients';
import {Company} from '@app/features/companies/models/Company';
import {SpoolExcel} from '@app/features/bill-spool/models/SpoolExcel';
import {VerifyExcelInterface} from '@app/features/bill-spool/interfaces/verify-excel.interface';


export const VerifyExcelAction = createAction(
  '[Spool] Verify Company client and Id bill',
  props<{signature: string, data: VerifyExcelInterface}>()
);

export const VerifyExcelSuccessAction = createAction(
  '[Spool] Verify Company client and Id bill success',
  props<{client: Clients, company: Company }>()
);


export const SendSpoolAction = createAction(
  '[Spool] Send Spool',
  props<{client: Clients, company: Company, form: any, excel: SpoolExcel}>()
);


export const SendSpoolSuccessAction = createAction(
  '[Spool] Send Spool success',
  props<{signature: string}>()
);
