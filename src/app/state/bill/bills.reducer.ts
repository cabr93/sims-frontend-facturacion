import {BillsState} from '@state/bill/index';
import {Action, createReducer, on} from '@ngrx/store';
import * as BillsActions from './bills.actions';
import {Clients} from '@app/features/client/models/Clients';
import {Company} from '@app/features/companies/models/Company';


const initialState: BillsState = {
  bills: []
};

const billReducer = createReducer(
  initialState,
  on(BillsActions.VerifyExcelSuccessAction, (state, {client, company }) => ({
    ...state,
    company,
    client
  }))
);

export function reducer(state: BillsState, action: Action) {
  return billReducer(state, action);
}
