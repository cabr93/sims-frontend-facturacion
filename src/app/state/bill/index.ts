import {Bills} from '@app/features/bill-spool/models/Bills';
import * as fromApp from './../app.state';
import {createFeatureSelector, createSelector} from '@ngrx/store';
import {Clients} from '@app/features/client/models/Clients';
import {Company} from '@app/features/companies/models/Company';


export interface BillsState {
  bills?: Bills[];
  client?: Clients;
  company?: Company;
}

export interface State extends fromApp.State {
  bills: BillsState;
}

export const getBillsState = createFeatureSelector<BillsState>('bills');

export const getBills = createSelector(
  getBillsState,
    state => state.bills
);

export const getClientCompany = createSelector(
  getBillsState,
  state =>  [state.client, state.company]
);
