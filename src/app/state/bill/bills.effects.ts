import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import * as BillsActions from './bills.actions';
import {catchError, exhaustMap, map} from 'rxjs/operators';
import {BillsService} from '@app/features/bill-spool/services/bills.service';
import * as fromApp from './../app.state';
import {Store} from '@ngrx/store';
import {ServicesSignatures} from '@shared/enums/services-signatures.enum';
import * as ServicesStatusActions from '@state/services-status/services-status.actions';
import { of } from 'rxjs';
import {HttpErrorResponse} from '@angular/common/http';


@Injectable()
export class BillsEffects {

  constructor(
    private actions$: Actions,
    private billsService: BillsService,
    private store: Store<fromApp.State>,
  ) {
  }

  $verifyExcel = createEffect( () =>
    this.actions$.pipe(
      ofType(BillsActions.VerifyExcelAction),
      exhaustMap( action =>
        this.billsService.verifyExcel(action.data).pipe(
          map( data => {
            console.log(data);
            this.store.dispatch(ServicesStatusActions.HideLoadingSuccessAction({ signature: ServicesSignatures.VERIFY_EXCEL }));
            return BillsActions.VerifyExcelSuccessAction( data);
          }),
          catchError((e: HttpErrorResponse) => {
            return of(ServicesStatusActions.HideLoadingErrorsAction({ signature: ServicesSignatures.VERIFY_EXCEL, error: '' }));
          })
        )
      )
    )
  );
  $sendSpool = createEffect( () =>
    this.actions$.pipe(
      ofType(BillsActions.SendSpoolAction),
      exhaustMap( action =>
        this.billsService.sendSpool(action).pipe(
          map( data => {
            this.store.dispatch(ServicesStatusActions.HideLoadingSuccessAction({ signature: ServicesSignatures.SEND_SPOOL }));
            return BillsActions.SendSpoolSuccessAction(data);
          }),
          catchError((e: HttpErrorResponse) => {
            return of(ServicesStatusActions.HideLoadingErrorsAction({ signature: ServicesSignatures.SEND_SPOOL, error: '' }));
          })
        )
      )
    )
  );
}

