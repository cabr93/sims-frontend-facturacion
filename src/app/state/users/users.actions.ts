import { createAction, props } from '@ngrx/store';
import {User} from '@core/models/User';

/**
 * Acciones que disparan efectos para obtener información de los servicios rest y
 * reducers para almacenar información de las alertas en el store.
 */

/**
 * Obtener todas las Compañias.
 */
export const GetUsersAction = createAction(
  '[Users] Get Users',
  props<{ signature: string }>()
);

/**
 * Guardar las Compañias en el Store.
 */
export const SetUsersAction = createAction(
  '[Users] Set Users',
  props<{ users: User[] }>()
);


export const EditUsersAction = createAction(
  '[Users] Edit user',
  props<{ users: User }>()
);

export const CreateUsersAction = createAction(
  '[Users] Create User',
  props<{ users: User }>()
);


export const DeleteUsersAction = createAction(
  '[Users] Delete User',
  props<{ _id: string, signature: string}>()
);
