import { Action, createReducer, on } from '@ngrx/store';
import * as UsersActions from './users.actions';
import {UsersState} from '@state/users/index';

/**
 * Inicialización del estado de las companies.
 */
const initialState: UsersState = {
  users: []
};

/**
 * Creación del reducer de companies.
 */
const usersReducer = createReducer(
  initialState,
  on(UsersActions.SetUsersAction, (state, { users }) => ({
    ...state,
    users
  }))
);

/**
 * Inicialización del reducer de alertas.
 */
export function reducer(state: UsersState, action: Action) {
  return usersReducer(state, action);
}

