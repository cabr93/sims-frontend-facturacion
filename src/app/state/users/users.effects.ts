import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {Store} from '@ngrx/store';
import {UsersState} from '@state/users/index';
import * as UsersActions from '@state/users/users.actions';
import {catchError, exhaustMap, map} from 'rxjs/operators';
import * as ServicesStatusActions from '@state/services-status/services-status.actions';
import {ServicesSignatures} from '@shared/enums/services-signatures.enum';
import {HttpErrorResponse} from '@angular/common/http';
import {of} from 'rxjs';
import {UserService} from '@app/features/user/services/user.service';
import * as ClientsActions from '@state/clients/clients.actions';

@Injectable()
export class UsersEffects {

  /**
   * @ignore
   */
  constructor(
    private actions$: Actions,
    private store: Store<UsersState>,
    private userService: UserService
  ) { }

  $getUsers = createEffect(() =>
    this.actions$.pipe(
      ofType( UsersActions.GetUsersAction),
      exhaustMap( () =>
        this.userService.getUsers().pipe(
          map( users => {
            this.store.dispatch(ServicesStatusActions.HideLoadingSuccessAction({ signature: ServicesSignatures.USERS }));
            return UsersActions.SetUsersAction({users});
          }),
          catchError( (e: HttpErrorResponse) => {
            return of(ServicesStatusActions.HideLoadingErrorsAction({ signature: ServicesSignatures.USERS, error: String(e.status) }));
          })
        )
      )
    )
  );
  $newUser = createEffect(() =>
    this.actions$.pipe(
      ofType( UsersActions.CreateUsersAction),
      exhaustMap( action =>
        this.userService.newUser(action.users).pipe(
          map( () => {
            this.store.dispatch(ServicesStatusActions.HideLoadingSuccessAction({ signature: ServicesSignatures.USERS_ADD }));
            return UsersActions.GetUsersAction({signature: ServicesSignatures.USERS});
          }),
          catchError( (e: HttpErrorResponse) => {
            // tslint:disable-next-line:max-line-length
            return of(ServicesStatusActions.HideLoadingErrorsAction({ signature: ServicesSignatures.USERS_ADD, error: String(e.status) }));
          })
        )
      )
    )
  );
  $editUser = createEffect(() =>
    this.actions$.pipe(
      ofType( UsersActions.EditUsersAction),
      exhaustMap( action =>
        this.userService.editUser(action.users).pipe(
          map( clients => {
            this.store.dispatch(ServicesStatusActions.HideLoadingSuccessAction({ signature: ServicesSignatures.USERS_ADD }));
            return UsersActions.GetUsersAction({signature: ServicesSignatures.USERS});
          }),
          catchError( (e: HttpErrorResponse) => {
            // tslint:disable-next-line:max-line-length
            return of(ServicesStatusActions.HideLoadingErrorsAction({ signature: ServicesSignatures.USERS_ADD, error: String(e) }));
          })
        )
      )
    )
  );
  $deleteUser = createEffect( () =>
    this.actions$.pipe(
      ofType( UsersActions.DeleteUsersAction),
      exhaustMap( action =>
        this.userService.deleteUser(action._id).pipe(
          map( () => {
            console.log('success');
            this.store.dispatch(ServicesStatusActions.HideLoadingSuccessAction({ signature: ServicesSignatures.USERS }));
            return UsersActions.GetUsersAction({signature: ServicesSignatures.USERS});
          }),
          catchError( (e: HttpErrorResponse) => {
            console.log('error');
            // tslint:disable-next-line:max-line-length
            return of(ServicesStatusActions.HideLoadingErrorsAction({ signature: ServicesSignatures.USERS, error: String(e) }));
          })
        )
      )
    )
  );
}
