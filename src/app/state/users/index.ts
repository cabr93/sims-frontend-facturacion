import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromApp from './../app.state';
import {User} from '@core/models/User';

/**
 * Estructura del estado de la sección de Companies.
 */
export interface UsersState {
  users: User[];
}

/**
 * Extención del estado principal.
 */
export interface State extends fromApp.State {
  users: UsersState;
}

/**
 * Selector para obtener la parte de los clientes del estado global de la aplicación.
 */
export const getUsersState = createFeatureSelector<UsersState>('users');

/**
 * Selector que retorna las clientes que están en el store.
 */
export const getUsers = createSelector(
  getUsersState,
  state => state.users
);
