import {createAction, props} from '@ngrx/store';


export const ShowLoadingAction = createAction(
  '[Loading] show loading',
  props<{ signature: string}>()
);

export const HideLoadingSuccessAction = createAction(
  '[Loading] Hide success loading',
  props<{signature: string}>()
);

export const HideLoadingErrorsAction = createAction(
  '[Loading] Hide error loading',
  props<{ signature: string, error: string}>()
);

export const RemoveStatusAction = createAction(
  '[Loading] Remove status',
  props<{signature: string}>()
);
