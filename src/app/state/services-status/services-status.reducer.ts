import {ServicesStatus} from '@state/services-status/services-status.interface';
import {Action, createReducer, on} from '@ngrx/store';

import * as ServicesStatusActions from './services-status.actions';

export interface ServicesState {
  services: Array<ServicesStatus>;
}

const initialState: ServicesState = {
  services: []
};

const itemExist = (items: ServicesStatus[], signature: string): ServicesStatus[] => {
  let exist = false;
  items = items.map( i => {
    if (i.signature === signature) {
      exist = true;
      return Object.assign({}, i, {signature, loading: true, loaded: false, error: ''});
    }
    return i;
  });

  if (exist) {
    return items;
  }

  items = [...items, {signature, loading: true, loaded: false, error: ''}];

  return items;
};

const servicesStatusReducer = createReducer(
  initialState,
  on(ServicesStatusActions.ShowLoadingAction, (state, {signature}) => ({
    ...state,
    services: state.services.length === 0
      ? [...state.services].concat([{ signature, loading: true, loaded: false, error: ''}])
      : itemExist(state.services, signature)
  })),
  on(ServicesStatusActions.HideLoadingSuccessAction, (state, { signature }) => ({
    ...state,
    services: [
      ...state.services.map( i => {
        if (i.signature === signature) {
          i = Object.assign({}, i, {signature, loading: false, loaded: true, error: ''});
        }
        return i;
      })
    ]
  })),
  on(ServicesStatusActions.HideLoadingErrorsAction, (state, { signature, error }) => ({
    ...state,
    services: [
      ...state.services.map( i => {
        if (i.signature === signature) {
          i = Object.assign({}, i, { signature, loading: false, loaded: false, error });
        }
        return i;
      })
    ]
  })),
  on(ServicesStatusActions.RemoveStatusAction, (state, { signature }) => ({
    ...state,
    services: [
      ...state.services.filter( i => i.signature !== signature)
    ]
  }))
);

export function reducer(state: ServicesState, action: Action) {
  return servicesStatusReducer(state, action);
}

