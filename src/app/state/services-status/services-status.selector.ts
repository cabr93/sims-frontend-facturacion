import { createFeatureSelector, createSelector } from '@ngrx/store';
import {ServicesState} from '@state/services-status/services-status.reducer';
import { ServicesStatus} from '@state/services-status/services-status.interface';

/**
 * Selector para obtener la parte del estado de los servicios del estado global de la aplicación.
 */
export const getServicesStatusState = createFeatureSelector<ServicesState>('servicesStatus');

/**
 * Obtener todos los  estados que están en el Store.
 */
export const getServicesStates = createSelector(
  getServicesStatusState,
  (state: ServicesState) => state.services
);

/**
 * Obtener el estado de un servicio por firma (signature)
 */
export const getServiceStateById = createSelector(
  getServicesStates,
  (state: Array<ServicesStatus>, props) => state.filter( i => i.signature === props.signature)[0]
);
