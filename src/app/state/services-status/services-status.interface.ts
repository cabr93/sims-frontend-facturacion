export interface ServicesStatus {
  signature: string;
  loading: boolean;
  loaded: boolean;
  error: string;
}
