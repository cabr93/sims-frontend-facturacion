import { createAction, props } from '@ngrx/store';
import {Historical} from '@app/features/historical/models/Historical';

/**
 * Obtener todas las Compañias.
 */
export const GetHistoricalAction = createAction(
  '[Historical] Get historical spool',
  props<{ signature: string }>()
);

/**
 * Guardar las Compañias en el Store.
 */
export const SetHistoricalAction = createAction(
  '[Historical] Set historical spool',
  props<{ historical: Historical[] }>()
);
