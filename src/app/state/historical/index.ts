import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromApp from './../app.state';
import {Historical} from '@app/features/historical/models/Historical';


export interface HistoricalState {
  historical: Historical[];
}


export interface State extends fromApp.State {
  companies: HistoricalState;
}

export const getHistoricalState = createFeatureSelector<HistoricalState>('historical');

export const getHistorical = createSelector(
  getHistoricalState,
  state => state.historical
);

