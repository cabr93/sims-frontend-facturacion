import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {HistoricalState} from '@state/historical/index';
import {Store} from '@ngrx/store';
import {HistoricalService} from '@app/features/historical/services/historical.service';
import * as HistoricalActions from '@state/historical/historical.actions';
import {catchError, exhaustMap, map} from 'rxjs/operators';
import {HttpErrorResponse} from '@angular/common/http';
import * as ServicesStatusActions from '@state/services-status/services-status.actions';
import {of} from 'rxjs';
import {ServicesSignatures} from '@shared/enums/services-signatures.enum';


@Injectable()
export class HistoricalEffects {
  constructor(
    private action$: Actions,
    private store: Store<HistoricalState>,
    private  historicalService: HistoricalService
  ) { }

  $getHistorical = createEffect( () =>
    this.action$.pipe(
      ofType( HistoricalActions.GetHistoricalAction),
      exhaustMap( () =>
        this.historicalService.getHistorical().pipe(
          map( historical => {
            this.store.dispatch(ServicesStatusActions.HideLoadingSuccessAction({ signature: ServicesSignatures.HISTORICAL }));
            return HistoricalActions.SetHistoricalAction({historical});
          }),
          catchError( (e: HttpErrorResponse) => {
            return of(ServicesStatusActions.HideLoadingErrorsAction({ signature: ServicesSignatures.HISTORICAL, error: String(e.status) }));
          })
        )
      )
    )
  );
}
