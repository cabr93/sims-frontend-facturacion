import { Action, createReducer, on } from '@ngrx/store';
import {HistoricalState} from '@state/historical/index';
import * as HistoricalActions from '@state/historical/historical.actions';


const initialState: HistoricalState = {
  historical: []
};

const historicalReducer = createReducer(
  initialState,
  on(HistoricalActions.SetHistoricalAction, (state, { historical }) => ({
    ...state,
    historical
  }))
);

export function reducer(state: HistoricalState, action: Action) {
  return historicalReducer(state, action);
}
