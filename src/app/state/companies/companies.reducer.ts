import { Action, createReducer, on } from '@ngrx/store';
import { CompaniesState } from '@state/companies';
import * as CompaniesActions from './companies.actions';

/**
 * Inicialización del estado de las companies.
 */
const initialState: CompaniesState = {
  companies: []
};

/**
 * Creación del reducer de companies.
 */
const companiesReducer = createReducer(
  initialState,
  on(CompaniesActions.SetCompaniesAction, (state, { companies }) => ({
    ...state,
    companies
  }))
);

/**
 * Inicialización del reducer de alertas.
 */
export function reducer(state: CompaniesState, action: Action) {
  return companiesReducer(state, action);
}

