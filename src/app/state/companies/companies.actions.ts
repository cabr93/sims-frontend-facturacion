import { createAction, props } from '@ngrx/store';
import {Company} from '@app/features/companies/models/Company';

/**
 * Acciones que disparan efectos para obtener información de los servicios rest y
 * reducers para almacenar información de las alertas en el store.
 */

/**
 * Obtener todas las Compañias.
 */
export const GetCompaniesAction = createAction(
  '[Companies] Get company',
  props<{ signature: string }>()
);

/**
 * Guardar las Compañias en el Store.
 */
export const SetCompaniesAction = createAction(
  '[Companies] Set company',
  props<{ companies: Company[] }>()
);


export const EditCompaniesAction = createAction(
  '[Companies] Edit company',
  props<{ company: Company }>()
);


export const CreateCompaniesAction = createAction(
  '[Companies] Create company',
  props<{ company: Company }>()
);

export const DeleteCompaniesAction = createAction(
  '[Companies] Delete company',
  props<{ _id: string, signature: string}>()
);
