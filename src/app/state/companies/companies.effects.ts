import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {Store} from '@ngrx/store';
import {CompaniesState} from '@state/companies/index';
import * as CompaniesActions from '@state/companies/companies.actions';
import {catchError, exhaustMap, map} from 'rxjs/operators';
import * as ServicesStatusActions from '@state/services-status/services-status.actions';
import {ServicesSignatures} from '@shared/enums/services-signatures.enum';
import {HttpErrorResponse} from '@angular/common/http';
import {of} from 'rxjs';
import {CompaniesService} from '@app/features/companies/services/companies.service';
import * as ClientsActions from '@state/clients/clients.actions';

@Injectable()
export class CompaniesEffects {
  /**
   * @ignore
   */
  constructor(
    private actions$: Actions,
    private store: Store<CompaniesState>,
    private companiesService: CompaniesService
  ) { }

  $getCompanies = createEffect(() =>
    this.actions$.pipe(
      ofType( CompaniesActions.GetCompaniesAction),
      exhaustMap( () =>
        this.companiesService.getCompanies().pipe(
          map( companies => {
            this.store.dispatch(ServicesStatusActions.HideLoadingSuccessAction({ signature: ServicesSignatures.COMPANY }));
            return CompaniesActions.SetCompaniesAction({companies});
          }),
          catchError( (e: HttpErrorResponse) => {
            return of(ServicesStatusActions.HideLoadingErrorsAction({ signature: ServicesSignatures.COMPANY, error: String(e.status) }));
          })
        )
      )
    )
  );
  $newCompany = createEffect(() =>
    this.actions$.pipe(
      ofType( CompaniesActions.CreateCompaniesAction),
      exhaustMap( action =>
        this.companiesService.newCompany(action.company).pipe(
          map( company => {
            this.store.dispatch(ServicesStatusActions.HideLoadingSuccessAction({ signature: ServicesSignatures.COMPANY_ADD }));
            return CompaniesActions.GetCompaniesAction({signature: ServicesSignatures.COMPANY});
          }),
          catchError( (e: HttpErrorResponse) => {
            // tslint:disable-next-line:max-line-length
            return of(ServicesStatusActions.HideLoadingErrorsAction({ signature: ServicesSignatures.COMPANY_ADD, error: String(e.status) }));
          })
        )
      )
    )
  );
  $editCompanies = createEffect(() =>
    this.actions$.pipe(
      ofType( CompaniesActions.EditCompaniesAction),
      exhaustMap( action =>
        this.companiesService.editCompany(action.company).pipe(
          map( company => {
            this.store.dispatch(ServicesStatusActions.HideLoadingSuccessAction({ signature: ServicesSignatures.COMPANY_ADD }));
            return CompaniesActions.GetCompaniesAction({signature: ServicesSignatures.COMPANY});
          }),
          catchError( (e: HttpErrorResponse) => {
            // tslint:disable-next-line:max-line-length
            return of(ServicesStatusActions.HideLoadingErrorsAction({ signature: ServicesSignatures.COMPANY_ADD, error: String(e) }));
          })
        )
      )
    )
  );
  $deleteCompany = createEffect( () =>
    this.actions$.pipe(
      ofType( CompaniesActions.DeleteCompaniesAction),
      exhaustMap( action =>
        this.companiesService.deleteCompany(action._id).pipe(
          map( () => {
            this.store.dispatch(ServicesStatusActions.HideLoadingSuccessAction({ signature: ServicesSignatures.COMPANY }));
            return CompaniesActions.GetCompaniesAction({signature: ServicesSignatures.COMPANY});
          }),
          catchError( (e: HttpErrorResponse) => {
            // tslint:disable-next-line:max-line-length
            return of(ServicesStatusActions.HideLoadingErrorsAction({ signature: ServicesSignatures.COMPANY, error: String(e) }));
          })
        )
      )
    )
  );
}
