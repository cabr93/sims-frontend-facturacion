import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromApp from './../app.state';
import {Company} from '@app/features/companies/models/Company';

/**
 * Estructura del estado de la sección de Companies.
 */
export interface CompaniesState {
  companies: Company[];
}

/**
 * Extención del estado principal.
 */
export interface State extends fromApp.State {
  companies: CompaniesState;
}

/**
 * Selector para obtener la parte de los clientes del estado global de la aplicación.
 */
export const getCompaniesState = createFeatureSelector<CompaniesState>('companies');

/**
 * Selector que retorna las clientes que están en el store.
 */
export const getCompanies = createSelector(
  getCompaniesState,
  state => state.companies
);
