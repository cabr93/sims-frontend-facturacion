import { createAction, props } from '@ngrx/store';
import {Clients} from '@app/features/client/models/Clients';

/**
 * Acciones que disparan efectos para obtener información de los servicios rest y
 * reducers para almacenar información de las alertas en el store.
 */

/**
 * Obtener todas las alertas.
 */
export const GetClientsAction = createAction(
  '[Clients] Get clients',
  props<{ signature: string }>()
);

/**
 * Guardar las alertas en el Store.
 */
export const SetClientsAction = createAction(
  '[Clients] Set clients',
  props<{ clients: Clients[] }>()
);


export const EditClientsAction = createAction(
  '[Clients] Edit clients',
  props<{ clients: Clients }>()
);


export const CreateClientsAction = createAction(
  '[Clients] Create clients',
  props<{ clients: Clients }>()
);

export const DeleteClientsAction = createAction(
  '[Clients] Delete client',
  props<{ _id: string, signature: string}>()
);
