import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromApp from './../app.state';
import {Clients} from '@app/features/client/models/Clients';

/**
 * Estructura del estado de la sección de clientes.
 */
export interface ClientsState {
  clients: Clients[];
}

/**
 * Extención del estado principal.
 */
export interface State extends fromApp.State {
  clients: ClientsState;
}

/**
 * Selector para obtener la parte de los clientes del estado global de la aplicación.
 */
export const getClientsState = createFeatureSelector<ClientsState>('clients');

/**
 * Selector que retorna las clientes que están en el store.
 */
export const getClients = createSelector(
  getClientsState,
  state => state.clients
);
