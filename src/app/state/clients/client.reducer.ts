import { Action, createReducer, on } from '@ngrx/store';
import * as ClientsActions from './clients.actions';
import { ClientsState } from '@state/clients';

/**
 * Inicialización del estado de las alertas.
 */
const initialState: ClientsState = {
  clients: []
};

/**
 * Creación del reducer de alertas.
 */
const clientsReducer = createReducer(
  initialState,
  on(ClientsActions.SetClientsAction, (state, { clients }) => ({
    ...state,
    clients
  }))
);

/**
 * Inicialización del reducer de alertas.
 */
export function reducer(state: ClientsState, action: Action) {
  return clientsReducer(state, action);
}
