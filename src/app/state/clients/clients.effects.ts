import { Injectable } from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import * as ClientsActions from './clients.actions';
import {catchError, exhaustMap, map} from 'rxjs/operators';
import {ClientsService} from '@app/features/client/services/clients.service';
import {ClientsState} from '@state/clients/index';
import {Store} from '@ngrx/store';
import {HttpErrorResponse} from '@angular/common/http';
import { of } from 'rxjs';
import * as ServicesStatusActions from '@state/services-status/services-status.actions';
import {ServicesSignatures} from '@shared/enums/services-signatures.enum';

/**
 * Efectos que reaccionan a las acciones disparadas desde los componentes del módulo de alertas.
 */
@Injectable()
export class ClientsEffects {
  /**
   * @ignore
   */
  constructor(
    private actions$: Actions,
    private store: Store<ClientsState>,
    private clientsService: ClientsService
  ) { }

  /**
   * Efecto que escucha la acción de obtener las clientes.
   */
  $getClients = createEffect(() =>
    this.actions$.pipe(
      ofType( ClientsActions.GetClientsAction),
      exhaustMap( () =>
        this.clientsService.getClients().pipe(
          map( clients => {
            return ClientsActions.SetClientsAction({clients});
          }),
          catchError( (e: HttpErrorResponse) => {
            return of(ServicesStatusActions.HideLoadingErrorsAction({ signature: ServicesSignatures.CLIENTS, error: String(e.status) }));
          })
        )
      )
    )
  );
  $newClients = createEffect(() =>
    this.actions$.pipe(
      ofType( ClientsActions.CreateClientsAction),
      exhaustMap( action =>
        this.clientsService.newClient(action.clients).pipe(
          map( clients => {
            this.store.dispatch(ServicesStatusActions.HideLoadingSuccessAction({ signature: ServicesSignatures.CLIENTS_ADD }));
            return ClientsActions.GetClientsAction({signature: ServicesSignatures.CLIENTS});
          }),
          catchError( (e: HttpErrorResponse) => {
            // tslint:disable-next-line:max-line-length
            return of(ServicesStatusActions.HideLoadingErrorsAction({ signature: ServicesSignatures.CLIENTS_ADD, error: String(e.status) }));
          })
        )
      )
    )
  );
  $editClients = createEffect(() =>
    this.actions$.pipe(
      ofType( ClientsActions.EditClientsAction),
      exhaustMap( action =>
        this.clientsService.editClient(action.clients).pipe(
          map( clients => {
            this.store.dispatch(ServicesStatusActions.HideLoadingSuccessAction({ signature: ServicesSignatures.CLIENTS_ADD }));
            return ClientsActions.GetClientsAction({signature: ServicesSignatures.CLIENTS});
          }),
          catchError( (e: HttpErrorResponse) => {
            // tslint:disable-next-line:max-line-length
            return of(ServicesStatusActions.HideLoadingErrorsAction({ signature: ServicesSignatures.CLIENTS_ADD, error: String(e) }));
          })
        )
      )
    )
  );
  $deleteClient = createEffect( () =>
    this.actions$.pipe(
      ofType( ClientsActions.DeleteClientsAction),
      exhaustMap( action =>
        this.clientsService.deleteClient(action._id).pipe(
          map( () => {
            console.log('success');
            this.store.dispatch(ServicesStatusActions.HideLoadingSuccessAction({ signature: ServicesSignatures.CLIENTS }));
            return ClientsActions.GetClientsAction({signature: ServicesSignatures.CLIENTS});
          }),
          catchError( (e: HttpErrorResponse) => {
            console.log('error');
            // tslint:disable-next-line:max-line-length
            return of(ServicesStatusActions.HideLoadingErrorsAction({ signature: ServicesSignatures.CLIENTS, error: String(e) }));
          })
        )
      )
    )
  );
}
