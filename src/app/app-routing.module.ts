import { NgModule } from '@angular/core';
import {Routes, RouterModule, PreloadAllModules} from '@angular/router';
import {NotFoundComponent} from '@core/pages/not-found/not-found.component';
import {AuthGuard} from '@core/guards/auth.guard';
import {IsLoginGuard} from '@core/guards/is-login.guard';


const routes: Routes = [
  { path: '', redirectTo: 'auth', pathMatch: 'full'},
  {
    path: 'auth',
    loadChildren: () => import('./core/authentication/authentication.module').then(m => m.AuthenticationModule),
    canLoad: [IsLoginGuard],
    canActivate: [IsLoginGuard]
  },
  {
    path: 'main',
    loadChildren: () => import('./features/features.module').then(m => m.FeaturesModule),
    canLoad: [ AuthGuard ],
    canActivate: [ AuthGuard ]
  },
  {
    path: '**',
    component: NotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
